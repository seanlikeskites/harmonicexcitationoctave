function out = sinClip(in, drive, threshold)
% A function which applies soft clipping using the sin function.
%
% Usage: out = sinClip(in, drive, threshold)
%
% Return Values:
% (out) is the clipped signal.
%
% Arguments:
% (in) is the input signal to be processed.
% (drive) is the gain applied prior to clipping.
% (threshold) is a vector containing the thresholds for positive and negative samples.


% apply the drive gain
amplifiedSignal = drive*in;

% get the thresholds
if length(threshold) == 1
	positiveThreshold = threshold;
	negativeThreshold = -threshold;
else
	positiveThreshold = max(threshold);
	negativeThreshold = min(threshold);
end

% clip the signal
signalPolarity = sign(amplifiedSignal);

troughIndicies = amplifiedSignal <= negativeThreshold;
lowerMiddleIndicies = (amplifiedSignal > negativeThreshold) & (amplifiedSignal <= 0);
upperMiddleIndicies = (amplifiedSignal < positiveThreshold) & (amplifiedSignal > 0);
peakIndicies = amplifiedSignal >= positiveThreshold;

troughSamples = troughIndicies*negativeThreshold;
lowerMiddleSamples = negativeThreshold*sin(pi*lowerMiddleIndicies.*amplifiedSignal/(2*negativeThreshold));
upperMiddleSamples = positiveThreshold*sin(pi*upperMiddleIndicies.*amplifiedSignal/(2*positiveThreshold));
peakSamples = peakIndicies*positiveThreshold;

out = troughSamples + lowerMiddleSamples + upperMiddleSamples + peakSamples;
