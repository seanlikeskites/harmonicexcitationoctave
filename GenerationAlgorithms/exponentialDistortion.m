function out = exponentialDistortion(in, exponent)
% A function which applies soft clipping using an exponential function.
%
% Usage: exponentialDistortion (in, exponent)
%
% Return Values:
% (out) is the clipped signal.
%
% Arguments:
% (in) is the input signal to be processed.
% (exponent) is the exponent to raise the signal to.

out = power(in, exponent);
