function p = percentageDesiredSignal (signal, desired)

signalSpectrum = abs(fft(signal));
desiredSpectrum = abs(fft(desired));

spectrumMask = desiredSpectrum > 0.001;

sharedFreqs = signalSpectrum.*spectrumMask;

p = 100 * sum(sharedFreqs) / sum(signalSpectrum);
