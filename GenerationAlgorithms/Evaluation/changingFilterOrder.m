fluteSampleFiles = dir('../../ProcessingAudio/VAMETest/LoudnessMatchedFluteSamples/*.wav');
numSamples = length(fluteSampleFiles);
figureNumber = 1;

if ~exist('Graphs')
	mkdir('Graphs');
end

for sample = 1:numSamples
	tempName = fluteSampleFiles(sample).name(1:end-4);
	if tempName(1) == 'P';
	disp(tempName);
		[tempSig, tempFs] = wavread(['../../ProcessingAudio/VAMETest/LoudnessMatchedFluteSamples/' tempName '.wav']);
		tempF0 = dftFundamental(tempSig, tempFs);
		disp([num2str(tempF0) ' => ' closestNote(tempF0)]);

		[initialFreqs, initialAmps] = findSpectralPartials(tempSig, tempFs);
		partialDisplacements = abs(initialFreqs - tempF0);
		[minDisplacement, minDisplacementIndex] = min(partialDisplacements);
		tempF0Amp = initialAmps(minDisplacementIndex);

		fflush(stdout);

		filterOrders = 1:10:201;
		numOrders = length(filterOrders);
		ssbInharmonicities = zeros(1, numOrders);
		iapInharmonicities = zeros(1, numOrders);
		quadInharmonicities = zeros(1, numOrders);
		ssbAmplitudes = zeros(1, numOrders);
		iapAmplitudes = zeros(1, numOrders);
		quadAmplitudes = zeros(1, numOrders);

		for i = 1:numOrders
			tempSSB = ssb(tempSig, tempFs, tempF0, 2, filterOrders(i));
%			tempAddSSB = tempSig + tempSSB;
%			tempAddSSB /= max(abs(tempAddSSB));
%			ssbInharmonicities(i) = inharmonicity(tempAddSSB, tempFs);
			ssbInharmonicities(i) = inharmonicity(tempSSB, tempFs, tempF0);

			[ssbFreqs, ssbAmps] = findSpectralPartials(tempSSB, tempFs);
			partialDisplacements = abs(ssbFreqs - tempF0*2);
			[minDisplacement, minDisplacementIndex] = min(partialDisplacements);
			ssbAmp = ssbAmps(minDisplacementIndex);
			ssbAmplitudes(i) = ssbAmp;

			tempIAP = iap(tempSig, tempFs, tempF0, 2, filterOrders(i));
%			tempAddIAP = tempSig + tempIAP;
%			tempAddIAP /= max(abs(tempAddIAP));
%			iapInharmonicities(i) = inharmonicity(tempAddIAP, tempFs);
			iapInharmonicities(i) = inharmonicity(tempIAP, tempFs, tempF0);

			[iapFreqs, iapAmps] = findSpectralPartials(tempIAP, tempFs);
			partialDisplacements = abs(iapFreqs - tempF0*2);
			[minDisplacement, minDisplacementIndex] = min(partialDisplacements);
			iapAmp = iapAmps(minDisplacementIndex);
			iapAmplitudes(i) = iapAmp;

			tempQuad = quadratureShiftHarmonic(tempSig, tempFs, tempF0, 2, filterOrders(i));
%			tempAddQuad = tempSig + tempQuad;
%			tempAddQuad /= max(abs(tempAddQuad));
%			quadInharmonicities(i) = inharmonicity(tempAddQuad, tempFs);
			quadInharmonicities(i) = inharmonicity(tempQuad, tempFs, tempF0);

			[quadFreqs, quadAmps] = findSpectralPartials(tempQuad, tempFs);
			partialDisplacements = abs(quadFreqs - tempF0*2);
			[minDisplacement, minDisplacementIndex] = min(partialDisplacements);
			quadAmp = quadAmps(minDisplacementIndex);
			quadAmplitudes(i) = quadAmp;
		end

		hideFigures(figureNumber);
		figure(figureNumber);
		plot(filterOrders, [ssbInharmonicities; iapInharmonicities; quadInharmonicities]);
		title(tempName);
		xlabel('Filter Order');
		legend('SSB', 'IAP', 'Quad');
		ylabel('Inharmonicity');
		print(['Graphs/Inharmonicity_' tempName '.png'], '-dpng', '-color');
		++figureNumber;

		hideFigures(figureNumber);
		figure(figureNumber);
		plot(filterOrders, [ssbAmplitudes; iapAmplitudes; quadAmplitudes; ones(1, numOrders) * tempF0Amp]);
		title(tempName);
		xlabel('Filter Order');
		legend('SSB', 'IAP', 'Quad', 'f0');
		ylabel('Amplitude');
		print(['Graphs/Amplitudes_' tempName '.png'], '-dpng', '-color');
		++figureNumber;
	end
end
