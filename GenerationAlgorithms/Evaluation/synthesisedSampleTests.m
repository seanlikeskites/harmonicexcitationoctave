harmonicAmplitudes = [1, 0, 0.4, 0.3, 0.5, 0, 0.2];

numHarmonics = length(harmonicAmplitudes);
harmonicEnvelopes = zeros(numHarmonics, 4);
harmonicEnvelopes(1, :) = [200, 100, 0.7, 300];
harmonicEnvelopes(3, :) = [200, 80, 0.7, 400];
harmonicEnvelopes(4, :) = [200, 70, 0.7, 400];
harmonicEnvelopes(5, :) = [200, 50, 0.7, 500];
harmonicEnvelopes(7, :) = [200, 30, 0.7, 500];

fs = 48000;
ts = 1/fs;
t = ts:ts:4;
sig = zeros(size(t));
f0 = 220;
signalLength = length(sig);

for k = 1:numHarmonics
	if harmonicAmplitudes(k) > 0
		tempADSR = num2cell(harmonicEnvelopes(k, :));
		tempEnvelope = adsrEnvelope(tempADSR{:}, signalLength, fs);
		sig += harmonicAmplitudes(k) * sin(2*pi*k*f0*t) .* tempEnvelope;
	end
end

sig /= max(abs(sig));
initialInharmonicicty = inharmonicity(sig, fs);
[freqs, amps] = findSpectralPartials(sig, fs);

f1IAP = iap(sig, fs, f0, 2, 21);
reconstructedIAP = sig + f1IAP;
reconstructedIAP /= max(abs(reconstructedIAP));
iapInharmonicity = inharmonicity(reconstructedIAP, fs);
[iapFreqs, iapAmps] = findSpectralPartials(reconstructedIAP, fs);

f1SSB = ssb(sig, fs, f0, 2, 21);
reconstructedSSB = sig + f1SSB;
reconstructedSSB /= max(abs(reconstructedSSB));
ssbInharmonicity = inharmonicity(reconstructedSSB, fs);
[ssbFreqs, ssbAmps] = findSpectralPartials(reconstructedSSB, fs);

f1DFT = phaseVocoderHarmonic(sig, fs, f0, 1024, 256, 2);
reconstructedDFT = sig + f1DFT;
reconstructedDFT /= max(abs(reconstructedDFT));
dftInharmonicity = inharmonicity(reconstructedDFT, fs);
[dftFreqs, dftAmps] = findSpectralPartials(reconstructedDFT, fs);
