function out = spectralShift(in, fs, f0, shiftFactor)
% A function which shifts all frequency components in a signal up by a given amount.
%
% Usage: out = spectralShift(in, fs, f0, shiftFactor)
%
% Return Values:
% (out) is the resultant `spectrally shifted' signal.
%
% Arguments:
% (in) is the signal to be shifted.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (shiftFactor) defines the amount the signal's frequency is shifted. The actual shift (in hertz) will be this value multiplied by the value of f0. 

% Make the complex sinusoid for shifting
len = length(in);
duration = len/fs;
ts = 1/fs;
t = ts:ts:duration;
shifter = exp(j*2*pi*f0*shiftFactor*t);

% Take the hilbert transform of the input
analytic = hilbert(in);

% Shift that signal
shifted = shifter.*analytic;

out = real(shifted);
