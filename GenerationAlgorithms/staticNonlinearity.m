function out = staticNonlinearity(in, fs, f0, nonlinearity, args, firLen)
% A function which uses a static nonlinearity to produce a set of harmonics for a signal.
%
% Usage: out = staticNonlinearity(in, fs, f0, firLen, nonlinearity)
%
% Return Values:
% (out) is the set of generated harmonics.
%
% Arguments:
% (in) is the input signal you wish to generate harmonics for.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (nonlinearity) is the handle of a function which provides the nonlinearity.
% (args) is a cell array containing the arguments for the nonlinearity function.
% (firLen) is the length of the fir kernel used to isolate the fundamental.

% isolate the fundamental
if nargin < 6
	fundamental = isolateFundamental(in, fs, f0);
else
	fundamental = isolateFundamental(in, fs, f0, firLen);
end

% apply the nonlinearity
out = nonlinearity(fundamental, args{:});
