function If0 = isolateFundamental(in, fs, f0, firLen)
% A function which low pass filters a signal in order to isolate its fundamental.
%
% Usage: If0 = isolateFundamental(in, fs, f0, firLen)
%
% Return Values:
% (If0) is the resultant `isolated' fundamental.
%
% Arguments:
% (in) is the input signal to be filtered.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (firLen) is an optional argument which sets the length of the filer kernel. If not defined the filter used will be a biquad lowpass filter.

if nargin < 4
	If0 = juceBiquad(in, fs, f0);
else
	% calculate the normalised cutoff frequency for the filter
	cutoff = 3*f0/fs;

	% calculate the filter kernel
	filterKernel = fir1(firLen-1, cutoff);
	% freqz(filterKernel, 1, 100);

	% apply the filter
	If0 = filter(filterKernel, 1, in);
end

%=================================================================
% HACKY SHIT THAT NEEDS REDOING
%=================================================================
%normalisedF0 = 2*pi*f0/fs;
%complexF0 = exp(i*normalisedF0);
%f0Response = 0;
%for k = 1:length(filterKernel)
%	f0Response += filterKernel(k)/(complexF0^(k-1));
%end
%phase = angle(f0Response);
