function out = hardClip(in, drive, threshold)
% A function to hard clip a signal.
%
% Usage: out = hardClip(in, drive, threshold)
%
% Return Values:
% (out) is the clipped signal.
%
% Arguments:
% (in) is the input signal to be processed.
% (drive) is the gain applied prior to clipping.
% (threshold) is a vector containing the thresholds for positive and negative samples.

% apply the drive gain
amplifiedSignal = drive*in;

% get the thresholds
if length(threshold) == 1
	positiveThreshold = threshold;
	negativeThreshold = -threshold;
else
	positiveThreshold = max(threshold);
	negativeThreshold = min(threshold);
end

% clip the signal
signalPolarity = sign(amplifiedSignal);

troughIndicies = amplifiedSignal <= negativeThreshold;
middleIndicies = (amplifiedSignal > negativeThreshold) & (amplifiedSignal < positiveThreshold);
peakIndicies = amplifiedSignal >= positiveThreshold;

troughSamples = troughIndicies*negativeThreshold;
middleSamples = middleIndicies.*amplifiedSignal;
peakSamples = peakIndicies*positiveThreshold;

out = troughSamples + middleSamples + peakSamples;
