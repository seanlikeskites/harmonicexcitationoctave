function out = rectify(in)
% A function to full wave rectify a signal.
%
% Usage: out = rectify(in)
%
% Return Values:
% (out) is the rectified signal.
%
% Arguments:
% (in) is the input signal to be processed.

out = abs(in);
