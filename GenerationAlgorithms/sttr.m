function out = sttr(in, window, step)
% A function implementing short-time time reversal.
%
% Usage: out = sttr(in, window, step)
%
% Return Values:
% (out) is the processed signal.
%
% Arguments:
% (in) is the input signal to be processed.
% (window) is the window function to use expressed as an array, this should have constant overlap add for the given step
% size.
% (step) is the step size to use.

signalLength = length(in);
windowLength = length(window);
out = zeros(1, signalLength);
lastFrame = signalLength - windowLength + 1;

for i = 1:step:lastFrame
	frameEnd = i + windowLength - 1;
	frame = window.*in(i:frameEnd);
	out(i:frameEnd) += fliplr(frame);
end
