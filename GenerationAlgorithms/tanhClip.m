function out = tanhClip(in, drive)
% A simple hyperbolic tangent soft clipping function.
%
% Usage: out = tanhClip(in, drive)
%
% Return Values:
% (out) is the clipped signal.
%
% Arguments:
% (in) is the input signal to be processed.
% (drive) is the gain applied prior to clipping.

% clip the signal
out = tanh(drive*in);
