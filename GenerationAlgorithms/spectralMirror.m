function out = spectralMirror(in, factor = 2)
% A function which uses upsampling to produce a spectral folding effect. A mirror image of the lower half of the spectrum is created in the upper half of the spectrum.
%
% Usage: out = spectralMirror(in)
% 
% Return Values:
% (out) is the resultant `spectrally folded' signal.
%
% Arguments:
% (in) is the signal to be processed.
% (factor) is the factor by which to downsample and upsample.

% low pass filter the signal to avoid aliasing
filterTime = -100:100;
sincFunction = sinc(filterTime/factor)/factor;
filterWindow = hann(201);
filterKernel = sincFunction.*filterWindow';
filtered = filter(filterKernel, 1, in);

% down sample and then upsample
out = zeros(1, length(filtered));
out(1:factor:end) = filtered(1:factor:end);

