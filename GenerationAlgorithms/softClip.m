function out = softClip(in, drive, threshold)
% A function to soft clip a signal.
%
% Usage: out = softClip(in, drive, threshold)
%
% Return Values:
% (out) is the clipped signal.
%
% Arguments:
% (in) is the input signal to be processed.
% (drive) is the gain applied prior to clipping.
% (threshold) is a vector containing the thresholds for positive and negative samples.

% apply the drive gain
amplifiedSignal = drive*in;

% get the thresholds
if length(threshold) == 1
	positiveThreshold = threshold;
	negativeThreshold = -threshold;
else
	positiveThreshold = max(threshold);
	negativeThreshold = min(threshold);
end

% clip the signal
signalPolarity = sign(amplifiedSignal);

troughIndicies = amplifiedSignal < negativeThreshold;
troughTransitionIndicies = (amplifiedSignal >= negativeThreshold) & (amplifiedSignal <= negativeThreshold/2);
middleIndicies = (amplifiedSignal > negativeThreshold/2) & (amplifiedSignal < positiveThreshold/2);
peakTransitionIndicies = (amplifiedSignal >= positiveThreshold/2) & (amplifiedSignal <= positiveThreshold);
peakIndicies = amplifiedSignal > positiveThreshold;

troughSamples = troughIndicies*negativeThreshold;
troughTransitionSamples = negativeThreshold*troughTransitionIndicies.*(1 - 4/3*(1 - abs(amplifiedSignal/negativeThreshold)).^2);
middleSamples = 4*middleIndicies.*amplifiedSignal/3;
peakTransitionSamples = positiveThreshold*peakTransitionIndicies.*(1 - 4/3*(1 - abs(amplifiedSignal/positiveThreshold)).^2);
peakSamples = peakIndicies*positiveThreshold;

out = troughSamples + troughTransitionSamples + middleSamples + peakTransitionSamples + peakSamples;
