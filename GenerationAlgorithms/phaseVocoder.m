function out = phaseVocoder(in, fs, pitchRatio, windowSize, hopSize, f0 = 0)
% A function which uses a phase vocoder to pitch shift a signal.
%
% Usage: out = phaseVocoder(in, fs, pitchRatio, windowSize, hopSize, f0)
% 
% Return Values:
% (out) is the pitch shifted signal.
%
% Arguments:
% (in) is the signal to be resampled.
% (fs) is the sample rate of that signal.
% (pitchRatio) is the ratio of output pitch to input pitch.
% (windowSize) is the length of each windowing frame in samples.
% (hopSize) is the number of samples from the original signal that separate the start of each frame.
% (f0) is an optional argument. If the fundamental frequency of the input signal is given the
%	fundamental will the isolated and shifted. The ouput is then a single generated harmonic
%	for the input signal

signalLength = length(in);

% separate the audio into frames
[frames, numFrames] = stftFrames(in, windowSize, hopSize);

% find whether the given pitch ratio is achievable
totalFramesLength = windowSize + (numFrames - 1) * hopSize;
maxPitchRatio = (totalFramesLength + (windowSize - hopSize) * (numFrames - 1)) / totalFramesLength;
minPitchRatio = (totalFramesLength - (hopSize - 1) * (numFrames - 1)) / totalFramesLength;
if not((minPitchRatio <= pitchRatio) && (pitchRatio <= maxPitchRatio))
	errorString = ["The given pitch ratio is not achievable with the given window and hop sizes."
	               "The value should be between " num2str(minPitchRatio) " and " num2str(maxPitchRatio) "."]
	error(errorString);
end

% dft bin frequencies for the window length
binFrequencies = 2 * pi * (0:(windowSize - 1)) / windowSize;

% calculate hops size for synthesis stage and new signal length
scaledSignalLength = round(signalLength * pitchRatio);
synthesisHopSize = round((scaledSignalLength - windowSize) / (numFrames - 1));
scaledSignalLength = (windowSize * numFrames) - (windowSize - synthesisHopSize) * (numFrames - 1);

% output gain
gain = synthesisHopSize / windowSize;

% windowing function for stft business
window = hann(windowSize);

previousPhases = zeros(1, windowSize);
synthesisPhases = zeros(1, windowSize);
out = zeros(1, scaledSignalLength);

for frameNumber = 1:numFrames
	% window frame data
	frameData = frames(frameNumber, :) .* window';

	% take the dft
	analysisSpectrum = fft(frameData);
	
	if f0
		% zero the bins which are not the fundamental frequency
		naturalF0 = 2 * pi * f0 / fs;
		aliasF0 = 2 * pi - naturalF0;
		fundamentalBins = ((naturalF0 - 0.1 < binFrequencies ) & (binFrequencies < naturalF0 + 0.1)) ...
				  | ((aliasF0 - 0.1 < binFrequencies ) & (binFrequencies < aliasF0 + 0.1));
		analysisSpectrum .*= fundamentalBins;
	end

	% calculate phases and magnitudes
	analysisMagnitudes = abs(analysisSpectrum);
	analysisPhases = angle(analysisSpectrum);

	% calculate phase change between frames
	phaseDeviation = analysisPhases - previousPhases;
	previousPhases = analysisPhases;

	% remove the expected phase difference
	adjustedPhases = phaseDeviation - hopSize * binFrequencies;
	wrappedPhases = mod(adjustedPhases + pi, 2 * pi) - pi;
	trueFrequencies = binFrequencies + wrappedPhases / hopSize;

	% calculate phase values for the synthesis stage
	synthesisPhases += synthesisHopSize * trueFrequencies;

	% synthesis the new audio frames
	[synthesisReal synthesisImag] = pol2cart(synthesisPhases, analysisMagnitudes);
	synthesisFrame = real(ifft(synthesisReal + i * synthesisImag)) .* window';

	% overlap add the synthsised frames
	synthesisFrameStart = (frameNumber - 1) * synthesisHopSize + 1;
	synthesisFrameEnd = synthesisFrameStart + windowSize - 1;
	out(synthesisFrameStart:synthesisFrameEnd) += synthesisFrame;
end

% resample to shift the pitch
resamplingRatio = signalLength / scaledSignalLength;
resamplingRatio = round(10 * resamplingRatio) / 10;
out = gain*changeSampleRate(out, resamplingRatio);
