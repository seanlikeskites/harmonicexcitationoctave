in = -1:0.001:1;
hardOut = hardClip(in, 1, 0.5);
softOut = softClip(in, 1, 0.5);

hideFigures(1);
plot(in, [hardOut;softOut], 'linewidth', 3);
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
legend('Hard Clipping', 'Soft Clipping');
xlabel('Input');
ylabel('Output');
axis([-1, 1, -1, 1]);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/Clipping.eps',
'-FArial:18', '-deps', '-color');

fs = 200000;
ts = 1/fs;
t = ts:ts:5;
sig = sin(2*pi*10*t);
hardClipped = hardClip(sig, 1, 0.1);
%hardClipped = hardClipped./(max(abs(hardClipped)));
softClipped = softClip(sig, 1, 0.1);
%softClipped = softClipped./(max(abs(softClipped)));

hideFigures(2);
fftPlot(hardClipped, fs, 'r', 1, 1, -200);
hold on;
fftPlot(softClipped, fs, 'b', 1, 1, -200);
hold off;

set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
axis([10, 100000, -200, 0]);
legend('Hard Clipping', 'Soft Clipping');
xlabel('Frequency (Hz)');
ylabel('Amplitude (dB)');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/ClippingSpectra.eps',
'-FArial:18', '-deps', '-color');

approx = polyfit(in, hardOut, 7);
approxHard = polyval(approx, in);

hideFigures(3);
plot(in, [hardOut;approxHard], 'linewidth', 3);
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
legend('Hard Clipping', '7^{th} Order Approximation');
xlabel('Input');
ylabel('Output');
axis([-1, 1, -1, 1]);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/ClippingApproximation.eps',
'-FArial:18', '-deps', '-color');

line = in;
len = length(sig);
freqs = fs*(0:(len-1)) / len;
freqs = freqs(51:100:end);
hardSpec = fft(2*hardClipped/len)(51:100:end);
softSpec = fft(2*softClipped/len)(51:100:end);
save('GenerationAlgorithms/Graphs/ClippingData.mat', 'freqs', 'hardSpec', 
     'softSpec', 'line', 'hardOut', 'softOut', 'approxHard');
