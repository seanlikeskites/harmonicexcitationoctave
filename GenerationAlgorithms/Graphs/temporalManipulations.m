fs = 44100;
ts = 1/fs;
t = ts:ts:7;
sig = sin(2*pi*t);

up = (1:(2*fs)) / (2*fs);
sig(1:fs) = 0;
sig((fs+1):(3*fs)) .*= up;
sig((4*fs+1):(6*fs)) .*= fliplr(up);
sig((6*fs+1):end) = 0;

clipped = sign(sig);

hideFigures(1);
plot(t, [sig; clipped], 'linewidth', 3);
set(gca, 'fontsize', 20);
axis([0, 7, -1.3, 1]);
axis off;
legend('Original Signal', 'Clipped Signal', 'location', 'south');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/InfinitePeakClipping.eps',
'-FArial:18', '-deps', '-color');

cubed = sig.^(3);
cubeRooted = sign(sig).*abs(sig).^(1/3);

hideFigures(2);
plot(t, [sig; cubed; cubeRooted], 'linewidth', 3);
set(gca, 'fontsize', 20);
axis([0, 7, -1.4, 1]);
axis off;
legend('Original Signal', 'Signal Cubed', 'Signal Cube Root', 'location', 'south');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/ExponentiationTemporalEffects.eps',
'-FArial:18', '-deps', '-color');

ssb = real(hilbert(sig).^(3));

hideFigures(3);
plot(t, [sig; ssb], 'linewidth', 3);
set(gca, 'fontsize', 20);
axis([0, 7, -1.4, 1]);
axis off;
legend('Original Signal', 'Third Order SSBA', 'location', 'south');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/SSBATemporalEffects.eps',
'-FArial:18', '-deps', '-color');

iap = abs(hilbert(sig)) .* cos(3*arg(hilbert(sig)));

hideFigures(4);
plot(t, [sig; iap], 'linewidth', 3);
set(gca, 'fontsize', 20);
axis([0, 7, -1.4, 1]);
axis off;
legend('Original Signal', 'Third Order IAP', 'location', 'south');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/IAPTemporalEffects.eps',
'-FArial:18', '-deps', '-color');
