fs = 192000;
ts = 1/fs;
t = ts:ts:5;
sig = sin(2*pi*1000*t) + 0.6*sin(2*pi*2000*t) + 0.5*sin(2*pi*3000*t) + 0.3*sin(2*pi*4000*t);
sig = sig./max(abs(sig));

cubed = sig.^3;
toTheTwoPointFive = sig.^2.5;

hideFigures(1);
fftPlot(toTheTwoPointFive, fs, 'g', 0, 1, -200, 30);
hold on;
fftPlot(sig, fs, 'b', 0, 1, -200, 5);
hold off;
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
xlabel('Frequency (Hz)');
ylabel('Amplitude (dB)');
legend('Signal Raised to the Power 2.5', 'Original Signal');
axis([0, 25000, -100, 20]);
set(gca, 'Layer', 'top', 'Clipping', 'on');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/RaisedToTwoAndAHalfSpectra.eps',
'-FArial:18', '-deps', '-color');

hideFigures(2);
fftPlot(cubed, fs, 'g', 0, 1, -200, 30);
hold on;
fftPlot(sig, fs, 'b', 0, 1, -200, 5);
hold off;
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
xlabel('Frequency (Hz)');
ylabel('Amplitude (dB)');
legend('Signal Cubed', 'Original Signal');
axis([0, 25000, -100, 20]);
set(gca, 'Layer', 'top', 'Clipping', 'on');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/CubedSpectra.eps',
'-FArial:18', '-deps', '-color');

singleSideBandCubed = real(hilbert(sig).^3);

hideFigures(3);
fftPlot(singleSideBandCubed, fs, 'g', 0, 1, -200, 30);
hold on;
fftPlot(sig, fs, 'b', 0, 1, -200, 5);
hold off;
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
xlabel('Frequency (Hz)');
ylabel('Amplitude (dB)');
legend('Third Order SSBA', 'Original Signal');
axis([0, 25000, -100, 20]);
set(gca, 'Layer', 'top', 'Clipping', 'on');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/SSBA3Spectra.eps',
'-FArial:18', '-deps', '-color');

iapCubed = abs(hilbert(sig)) .* cos(3*arg(hilbert(sig)));

hideFigures(4);
fftPlot(iapCubed, fs, 'g', 0, 1, -200, 30);
hold on;
fftPlot(sig, fs, 'b', 0, 1, -200, 5);
hold off;
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
xlabel('Frequency (Hz)');
ylabel('Amplitude (dB)');
legend('Third Order IAP', 'Original Signal');
axis([0, 25000, -100, 20]);
set(gca, 'Layer', 'top', 'Clipping', 'on');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/IAP3Spectra.eps',
'-FArial:18', '-deps', '-color');

len = length(sig);
freqs = fs*(0:(len-1)) / len;
freqs = freqs(1:100:end);
sigSpec = fft(2*sig/len)(1:100:end);
cubeSpec = fft(2*cubed/len)(1:100:end);
twopfiveSpec = fft(2*toTheTwoPointFive/len)(1:100:end);
twopfiveSpec(1) /= 2;
ssbSpec = fft(2*singleSideBandCubed/len)(1:100:end);
iapSpec = fft(2*iapCubed/len)(1:100:end);
save('GenerationAlgorithms/Graphs/ComplexSpectra.mat', 'freqs', 'sigSpec',
     'cubeSpec', 'twopfiveSpec', 'ssbSpec', 'iapSpec');
