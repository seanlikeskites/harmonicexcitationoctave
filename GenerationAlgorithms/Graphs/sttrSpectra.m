fs = 192000;
ts = 1/fs;
t = ts:ts:5;
sig = sin(2*pi*1000*t);
sig = sig./max(abs(sig));

% design the window
windowLength = fs/1000;
stepSize = windowLength/2;
window2 = sin(linspace(pi/6, pi/2, windowLength/4 + 1));
window1 = 1 - fliplr(window2);
window1(end) = [];
window = [window1, window2, fliplr(window2), fliplr(window1)];

hideFigures(1);
plot(window);
set(gca, 'fontsize', 20, 'xticklabel', {'-L/2', '0', 'L/2'}, 'xtick', [0, length(window)/2, length(window)]);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
set(gca, 'Layer', 'top', 'Clipping', 'on');
axis([0, length(window), 0, 1]);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/STTRWindow.eps',
'-FArial:18', '-deps', '-color');

saveWindow = window;

processed1ms = sttr(sig, window, stepSize);
processed1ms = processed1ms(windowLength:(end - windowLength + 1));

windowLength = 1.5*fs/1000;
stepSize = windowLength/2;
window2 = sin(linspace(pi/6, pi/2, windowLength/4 + 1));
window1 = 1 - fliplr(window2);
window1(end) = [];
window = [window1, window2, fliplr(window2), fliplr(window1)];

processed1p5ms = sttr(sig, window, stepSize);
processed1p5ms = processed1p5ms(windowLength:(end - windowLength + 1));

hideFigures(2);
fftPlot(processed1p5ms, fs, 'g', 0, 1, -200, 5);
hold on;
fftPlot(processed1ms, fs, 'r', 0, 1, -200, 5);
fftPlot(sig, fs, 'b', 0, 1, -200, 5);
hold off;
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
xlabel('Frequency (Hz)');
ylabel('Amplitude (dB)');
legend('1.5ms Window', '1ms Window', 'Original Signal');
axis([0, 25000, -100, 20]);
set(gca, 'Layer', 'top', 'Clipping', 'on');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/Graphs/STTRSpectra.eps',
'-FArial:18', '-deps', '-color');

len = length(sig);
freqs = fs*(0:(len-1)) / len;
saveIdx = freqs <= 25000;
freqs = freqs(saveIdx);
sigSpec = fft(2*sig/len)(saveIdx);
ms1Spec = fft(2*processed1ms/len)(saveIdx);
ms1p5Spec = fft(2*processed1p5ms/len)(saveIdx);
ms1p5Spec(1) /= 2;
save('GenerationAlgorithms/Graphs/STTRData.mat', 'saveWindow', 'freqs', 'sigSpec', 'ms1Spec', 'ms1p5Spec');
