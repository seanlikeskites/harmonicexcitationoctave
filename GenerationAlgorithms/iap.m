function Hn = iap(in, fs, f0, harmonic, firLen)
% A function which uses instantaneous amplitude and phase to generate a given harmonic for the input signal. 
%
% Usage: Hn = iap(in, fs, f0, harmonic, firLen)
%
% Return Values:
% (Hn) is the generated harmonic.
%
% Arguments:
% (in) is the input signal you wish to generate a harmonic for.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (harmonic) is the order of harmonic you wish to generate.
% (firLen) is the length of the fir kernel used to isolate the fundamental.

% isolate the fundamental and find its analytic representation
if nargin < 5
	fundamental = isolateFundamental(in, fs, f0);
else
	fundamental = isolateFundamental(in, fs, f0, firLen);
end
analyticFundamental = iirHilbert(fundamental);

% calculate amplitude and phase envelopes
amplitudeEnvelope = abs(analyticFundamental);
phaseEnvelope = angle(analyticFundamental);

% make sure the harmonic value is an integer
harmonic = fix(harmonic);

% generate the harmonic
Hn = amplitudeEnvelope.*cos(harmonic*phaseEnvelope);
