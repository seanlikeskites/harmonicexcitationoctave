function Hn = dftBinShift(in, fs, f0, harmonic)
% Broken, broken, horribly broken.

signalLength = length(in);

spectrum = fft(in);
binFrequencies = 2 * pi * (0:(signalLength - 1)) / signalLength;

naturalF0 = 2 * pi * f0 / fs;
aliasF0 = 2 * pi - naturalF0;
fundamentalBins = ((naturalF0 - 0.1 < binFrequencies ) & (binFrequencies < naturalF0 + 0.1));
spectrum .*= fundamentalBins;

fundamentalIndicies = find(fundamentalBins);
harmonicBins = (fundamentalIndicies - 1) * harmonic + 1;
harmonicBins = harmonicBins(harmonicBins < signalLength);
harmonicBins = harmonicBins(binFrequencies(harmonicBins) < pi);

newSpectrum = zeros(size(spectrum));
newSpectrum(harmonicBins) = spectrum((harmonicBins - 1) / harmonic + 1);

numReflectedBins = floor((signalLength - 1) / 2);
newSpectrum(end - numReflectedBins + 1:end) = fliplr(conj(newSpectrum(2:numReflectedBins + 1)));

Hn = real(ifft(newSpectrum));
