function Hn = quadratureShiftHarmonic(in, fs, f0, partial, firLen)
% A function which uses a quadrature frequency shift to generate a given partial for the input signal. 
%
% Usage: Hn = quadratureShiftHarmonic(in, fs, f0, partial, firLen)
%
% Return Values:
% (Hn) is the generated harmonic.
%
% Arguments:
% (in) is the input signal you wish to generate a harmonic for.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (partial) is the multiple of the fundamental frequency the generated partial should be.
% (firLen) is the length of the fir kernel used to isolate the fundamental.

if size(in, 1) > 1
	in = in';
end

% isolate the fundamental and find its analytic representation
if nargin < 5
	fundamental = isolateFundamental(in, fs, f0);
else
	fundamental = isolateFundamental(in, fs, f0, firLen);
end
analyticFundamental = iirHilbert(fundamental);

% make a complex sinusoid
signalLength = length(in);
t = (0:signalLength - 1) / fs;
shift = partial - 1;
shifterSignal = exp(2*i*pi*t*shift*f0);

% shift it 
offsetPartial = real(analyticFundamental.*shifterSignal);
Hn = offsetPartial - mean(offsetPartial);
