function out = exponentialClip(in, drive, threshold, exponent)
% A function which applies soft clipping using an exponential function.
%
% Usage: exponentialClip(in, drive, threshold, exponent)
%
% Return Values:
% (out) is the clipped signal.
%
% Arguments:
% (in) is the input signal to be processed.
% (drive) is the gain applied prior to clipping.
% (threshold) is a vector containing the thresholds for positive and negative samples.
% (exponent) is a vector containing the exponents for positive and negative samples.


% apply the drive gain
amplifiedSignal = drive*in;

% get the thresholds
if length(threshold) == 1
	positiveThreshold = threshold;
	negativeThreshold = -threshold;
else
	positiveThreshold = max(threshold);
	negativeThreshold = min(threshold);
end

% get the exponents
if length(exponent) == 1
	positiveExponent = exponent;
	negativeExponent = exponent;
else
	positiveExponent = exponent(1);
	negativeExponent = exponent(2);
end

% clip the signal
signalPolarity = sign(amplifiedSignal);

troughIndicies = amplifiedSignal <= negativeThreshold;
lowerMiddleIndicies = (amplifiedSignal > negativeThreshold) & (amplifiedSignal <= 0);
upperMiddleIndicies = (amplifiedSignal < positiveThreshold) & (amplifiedSignal > 0);
peakIndicies = amplifiedSignal >= positiveThreshold;

troughSamples = troughIndicies*negativeThreshold;

lowerMiddleSamples = lowerMiddleIndicies.*amplifiedSignal;
lowerMiddleClipped = negativeThreshold*(1 - abs(lowerMiddleSamples/negativeThreshold - 1).^negativeExponent);

upperMiddleSamples = upperMiddleIndicies.*amplifiedSignal;
upperMiddleClipped = positiveThreshold*(1 - abs(upperMiddleSamples/positiveThreshold - 1).^positiveExponent);

peakSamples = peakIndicies*positiveThreshold;

out = troughSamples + lowerMiddleClipped + upperMiddleClipped + peakSamples;
