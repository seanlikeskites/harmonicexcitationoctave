function out = plotHarmonicsVsAmplitude(inc, f0, func, args, orders)
% A function which plots the levels of individual harmonics against input amplitude for a system.
%
% Usage: graph = plotHarmonicVsAmplitude(inc, f0, func, args, orders)
% 
% Return Values:
% (graph) is the handle for the plotted graph.
%
% Arguments:
% (inc) is the value to increment the input amplitude by. From inc in step of inc to 1.
% (f0) is the fundamental frequency of that signal.
% (func) is a handle to the functin to be applied.
% (args) is a cell array with the parameter values for the function to be applied.
% (orders) is a vactor of the orders of harmoncis to plot.

% generate signal
fs = 96000;
ts = 1/fs;
t = ts:ts:0.05;
sig = sin(2*pi*f0*t);
maxHarmonic = floor(48000/f0);

% check arguments
if length(args) ~= nargin(func) - 1
	error('Wrong number of arguments!')
end

% set loop stuff
resolution = 1/inc;
levels = inc:inc:1;
distortionLevels = zeros(maxHarmonic, resolution);

% find distortion levels
for k = 1:resolution
	tempSig = levels(k)*sig;
	tempClip = func(tempSig, args{:});
	tempLevels = harmonicLevels(tempClip, fs, f0, 2);
	tempLevels = tempLevels / sqrt(sum(tempLevels.^2));
	distortionLevels(:, k) = tempLevels;
end

done = distortionLevels;

distortionLevels(distortionLevels < 10e-10) = 0;
distortionLevels = 20*log10(distortionLevels);

graph = plot(levels, distortionLevels(orders, :), 'linewidth', 3);
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
xlabel('Peak Input Amplitude');
ylabel('Distortion Level (dB)');

out = distortionLevels;
