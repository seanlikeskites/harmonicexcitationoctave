hideFigures(1);
hardClipHarms = plotHarmonicsVsAmplitude(0.005, 2000, @hardClip, {1, 0.5}, 3:2:13);
legend('h_{3}', 'h_{5}', 'h_{7}', 'h_{9}', 'h_{11}', 'h_{13}', 'location', 'northwest');
ylim ([-120, 0]);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/HarmonicsVsAmplitude/HardClippingHarmonics.eps', '-FArial:18', '-deps', '-color');

hideFigures(2);
softClipHarms = plotHarmonicsVsAmplitude(0.005, 2000, @softClip, {1, 0.5}, 3:2:13);
legend('h_{3}', 'h_{5}', 'h_{7}', 'h_{9}', 'h_{11}', 'h_{13}', 'location', 'northwest');
ylim ([-120, 0]);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/HarmonicsVsAmplitude/SoftClippingHarmonics.eps', '-FArial:18', '-deps', '-color');

hideFigures(3);
expClipHarms = plotHarmonicsVsAmplitude(0.005, 2000, @exponentialClip, {1, 0.5, 5}, 3:2:13);
legend('h_{3}', 'h_{5}', 'h_{7}', 'h_{9}', 'h_{11}', 'h_{13}', 'location', 'southeast');
ylim ([-120, 0]);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/HarmonicsVsAmplitude/ExponentialClippingHarmonics.eps', '-FArial:18', '-deps', '-color');

hideFigures(4);
asymmetricClipHarms = plotHarmonicsVsAmplitude(0.005, 2000, @hardClip, {1, [0.5, -0.3]}, 2:7);
legend('h_{2}', 'h_{3}', 'h_{4}', 'h_{5}', 'h_{6}', 'h_{7}', 'location', 'northwest');
ylim ([-120, 0]);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/HarmonicsVsAmplitude/AsymmetricHardClippingHarmonics.eps', '-FArial:18', '-deps', '-color');

hideFigures(5);
plotHarmonicsVsAmplitude(0.005, 2000, @exponentialDistortion, {3}, 2:7);
legend('h_{2}', 'h_{3}', 'h_{4}', 'h_{5}', 'h_{6}', 'h_{7}', 'location', 'northwest');
ylim ([-120, 0]);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/GenerationAlgorithms/HarmonicsVsAmplitude/SquaringHarmonics.eps', '-FArial:18', '-deps', '-color');

save('GenerationAlgorithms/HarmonicsVsAmplitude/HarmonicLevels.mat', 'hardClipHarms', 'softClipHarms', 'expClipHarms', 'asymmetricClipHarms');
