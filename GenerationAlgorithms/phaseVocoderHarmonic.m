function Hn = phaseVocoderHarmonic(in, fs, f0, windowSize, hopSize, harmonic, maxShiftRatio = 1.5)
% A function which uses a phase vocoder to generate a given harmonic for the input signal. 
%
% Usage: Hn = phaseVocoderHarmonic(in, fs, f0, windowSize, hopSize, harmonic, maxShiftRatio)
%
% Return Values:
% (Hn) is the generated harmonic.
%
% Arguments:
% (in) is the input signal you wish to generate a harmonic for.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (windowSize) is the length of each windowing frame in samples.
% (hopSize) is the number of samples from the original signal that separate the start of each frame.
% (harmonic) is the order of harmonic you wish to generate.
% (maxShiftRatio) is an optional argument which sets the maximum shift ratio used in a phase vocoder stage.

% caculate the number of phase vocoder stages needed
stageShiftRatio = harmonic;
numStages = 1;
while stageShiftRatio > maxShiftRatio
	++numStages;
	stageShiftRatio = nthroot(harmonic, numStages);
end

% process the signal
Hn = in;
for stageIndex = 0:numStages - 1
	Hn = phaseVocoder(Hn, fs, stageShiftRatio, windowSize, hopSize, f0 * stageShiftRatio ^ stageIndex);
end

% make it the length of the original input
originalLength = length(in);
processedLength = length(Hn);
if originalLength < processedLength
	Hn = Hn(1:originalLength);
else
	Hn = [Hn, zeros(1, originalLength - processedLength)];
end
