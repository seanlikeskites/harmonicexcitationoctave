load samples.mat

hideFigures(1);
myspectrogram(bass, 1024, 44100);
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/CelloSpectrogram.eps', '-FArial:18', '-deps', '-color');

hideFigures(2);
myspectrogram(bassFilt, 1024, 44100);
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/CelloFilteredSpectrogram.eps', '-FArial:18', '-deps', '-color');

hideFigures(3);
myspectrogram(bassSSB, 1024, 44100);
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/CelloSSBASpectrogram.eps', '-FArial:18', '-deps', '-color');

hideFigures(4);
myspectrogram(bassIAP, 1024, 44100);
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/CelloIAPSpectrogram.eps', '-FArial:18', '-deps', '-color');

hideFigures(5);
myspectrogram(bassSTFT, 1024, 44100);
set(gca, 'fontsize', 20);
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/CelloSynthesisSpectrogram.eps', '-FArial:18', '-deps', '-color');
