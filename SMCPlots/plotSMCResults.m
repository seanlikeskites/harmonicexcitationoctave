% plot the results
load('MushraResults.mat')

for k = 1:4
	means = res(k, :);
	hideFigures(k);
	bar(means);
	xValues = 1:11;
	errorValues = means - conf(k, :, 1);
	plottedXValues = xValues(errorValues>0.5);
	plottedErrorValues = errorValues(errorValues>0.5);
	plottedMeans = means(errorValues>0.5);
	hold on;
	h = errorbar(plottedXValues, plottedMeans, plottedErrorValues, '.r');
	set(h, 'linewidth', 3, 'markersize', 0.1);
	hold off;
	set(gca, 'fontsize', 20);
	set(get(gca, 'xlabel'), 'fontsize', 20);
	set(get(gca, 'ylabel'), 'fontsize', 20);
	boundaries = axis;
	axis([boundaries(1:2) 0 120]);
	ylabel('Normalised Grade', 'FontSize', 20);
	xlabel('Stimulus Number', 'FontSize', 20);
end

hideFigures(1);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/CelloResults.eps', '-FArial:18', '-deps', '-color');
hideFigures(2);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/SynthResults.eps', '-FArial:18', '-deps', '-color');
hideFigures(3);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/PianoResults.eps', '-FArial:18', '-deps', '-color');
hideFigures(4);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/ClarinetResults.eps', '-FArial:18', '-deps', '-color');

% plot the rnonlin stuff
load('RNonlin_Results');

for k = 1:4
	hideFigures(k+10);
	bar(results(k, :));
	mm = axis;
	axis([mm(1) mm(2) 0 120]);
	set(gca, 'fontsize', 20);
	set(get(gca, 'xlabel'), 'fontsize', 20);
	set(get(gca, 'ylabel'), 'fontsize', 20);
	xlabel('Stimulus Number', 'FontSize', 20);
	ylabel('Normalised R_{nonlin}', 'FontSize', 20);
end

hideFigures(11);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/CelloRNonlin.eps', '-FArial:18', '-deps', '-color');
hideFigures(12);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/SynthRNonlin.eps', '-FArial:18', '-deps', '-color');
hideFigures(13);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/PianoRNonlin.eps', '-FArial:18', '-deps', '-color');
hideFigures(14);
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/SMCPlots/ClarinetRNonlin.eps', '-FArial:18', '-deps', '-color');

