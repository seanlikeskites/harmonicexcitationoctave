function [Op Ap f0] = relativeHarmonicAmplitudes(in, fs, graph = false, quantise = false)
% A function which normalises the levels of harmonics in a given signal.
%
% Usage: [Op Ap f0] = relativeHarmonicAmplitudes(in, fs, graph, quantise)
% 
% Return Values:
% (Op) is a vector containing the orders of the harmonic partials.
% (Ap) is a vector containing the normalised amplitudes of the partials.
% (f0) is the calculated fundamental frequency of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (graph) is an optional boolean argument which controls whether a graph of the amplitudes is plotted.
% (quantise) is an optional boolean argument to exclude any inharmonic partials and quantise the orders of those that are close to harmonic.

% find the fundamental and levels and frequencies of partials
f0 = dftFundamental(in, fs);
if f0 == 0
	f0 = zeroCrossingFundamental(in, fs);
end
[freqs, amps] = findSpectralPartials(in, fs);

% calculate the orders of the partials
Op = freqs/f0;

% normalise the amplitudes
Ap = amps/max(amps);

% quantise harmonics
if quantise
	roundedOrders = round(Op);
	harmonicProximity = Op./roundedOrders;
	harmonicIndicies = (0.99 < harmonicProximity) & (harmonicProximity < 1.01);
	Op = roundedOrders(harmonicIndicies);
	Ap = Ap(harmonicIndicies);
end

% plot a graph
if graph
	bar(Op, Ap);
	xlabel('Overtone');
	ylabel('Relative Amplitude');
end


