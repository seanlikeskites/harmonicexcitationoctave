library(XML)
library(psy)
library(psych)

confidenceInterval <- function(data)
{
	dataMean <- mean(data)
	dataSD <- sd(data)
	n <- length(data)

	distributionValue <- qt(0.975, n - 1)
	displacement <- distributionValue * dataSD / sqrt(n)	

	lower <- dataMean - displacement
	upper <- dataMean + displacement

	return(c(lower, upper))
} 
  
analyseMSTR <- function(pathToFile, colourVector="black")
{ 
         # load in the file
         xmlDocument <- xmlTreeParse(pathToFile)
         rootNode <- xmlRoot(xmlDocument)
         numSessions <- xmlSize(rootNode)
  
	adjectives <- list()

	# get adjectives from each session
	for (i in 1:numSessions)
	{
		tempSessionNode <- rootNode[[i]]
		tempNumAdjectives <- xmlSize(tempSessionNode)

		if (tempNumAdjectives > 0)
		{
			for (k in 1:tempNumAdjectives)
			{
				adjectives <- c(adjectives, xmlName(tempSessionNode[[k]]))
			}
		}
	}

	# remove any repeated adjectives
	adjectives <- unique(adjectives)
	numAdjectives <- length(adjectives)

	# find all the sessions which have been completed
	sessionSizes <- xmlSApply(rootNode, xmlSize)
	fullSessions <- rootNode[sessionSizes == numAdjectives]
	numFullSessions <- length(fullSessions)
	numSamples <- length(xmlAttrs(fullSessions[[1]][[1]]))

	fullResults <- array(0, c(numSamples, numAdjectives, numFullSessions))

	# get the results out of the session
	for (i in 1:numFullSessions)
	{
		tempSessionNode <- fullSessions[[i]]
		tempResults <- xmlSApply(tempSessionNode, xmlAttrs)
		tempResults <- tempResults[,sort(colnames(tempResults))]
		tempNumeric <- matrix(as.numeric(tempResults), ncol=numAdjectives)
		fullResults[,,i] <- tempNumeric;
	}

	# sort the results into separate adjectives
	resultsByAdjective <- aperm(fullResults, c(3, 1, 2))

	reliabilities <- array(0, numAdjectives)

	# calculate cronbach alphas
	if (numSessions > 1)
	{
		for (i in 1:numAdjectives)
		{
			reliabilities[i] <- cronbach(resultsByAdjective[,,i])$alpha
		}
	}

	# find mean gradings across subjects
	meanGradings <- apply(fullResults, c(1, 2), mean)
	deviationOfGradings <- apply(fullResults, c(1, 2), sd)

	# do some MDS
	distances <- dist(meanGradings)
	scaledGradings <- cmdscale(distances, eig=TRUE, k=2)

	# find correlations between different adjectives
	adjectiveCorrelations <- cor(meanGradings)

	sortedAdjectives <- sort(as.character(adjectives))

	xs <- scaledGradings$points[,1]
	ys <- scaledGradings$points[,2]

	correlationThreshold <- 0.9
	
	# how well do adjectives correlate with MDS axes
	xAxisCorrelations <- cor(meanGradings, xs)
	negativeXCorrelations <- sortedAdjectives[xAxisCorrelations < -correlationThreshold]
	if (length(negativeXCorrelations) > 0)
	{
		negativeXCorrelations <- paste('-', negativeXCorrelations, sep='')
	}
	positiveXCorrelations <- sortedAdjectives[xAxisCorrelations > correlationThreshold]
	allXCorrelations <- c(positiveXCorrelations, negativeXCorrelations)
	xLabel <- paste(allXCorrelations, collapse=', ')

	yAyisCorrelations <- cor(meanGradings, ys)
	negativeYCorrelations <- sortedAdjectives[yAyisCorrelations < -correlationThreshold]
	if (length(negativeYCorrelations) > 0)
	{
		negativeYCorrelations <- paste('-', negativeYCorrelations, sep='')
	}
	positiveYCorrelations <- sortedAdjectives[yAyisCorrelations > correlationThreshold]
	allYCorrelations <- c(positiveYCorrelations, negativeYCorrelations)
	yLabel <- paste(allYCorrelations, collapse=', ')

	# plot that stuff
	plot(xs, ys, type='n', xlab=xLabel, ylab=yLabel, main="A Nice Graph")
	text(xs, ys, 1:numSamples, col=colourVector)
	dev.off()

	# confidences
	confidences <- apply(fullResults, c(1, 2), confidenceInterval)

	return(list(reliabilities, scaledGradings, adjectiveCorrelations, sortedAdjectives, resultsByAdjective, confidences))
}
