/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainContentComponent::MainContentComponent()
    : continueButton ("Continue"),
      instructionText ("", "")
{
    startTime = Time::getCurrentTime();

    // so we can read some .wav files in
    formatManager.registerBasicFormats();

    // open up a test xml file
    FileChooser chooser ("Select A Listening Test Config ...", File::nonexistent, "*.mst");

    if (chooser.browseForFileToOpen())
    {
        File configFile (chooser.getResult());
        XmlDocument configDoc (configFile);
        testInfo = configDoc.getDocumentElement();
            
        forEachXmlChildElement (*testInfo, section)
        {
            if (section->hasTagName ("AUDIO_FILE"))
            {
                // open up all the audio files needed
                String audioFilePath = section->getStringAttribute ("path");
                File audioFile (audioFilePath);
                AudioFormatReader* reader = formatManager.createReaderFor (audioFile);

                if (reader != nullptr)
                {
                    audioFileSources.add (new AudioFormatReaderSource (reader, true));
                }
            }

            if (section->hasTagName ("ADJECTIVE"))
            {
                // read in the adjectives
                adjectives.add (section->getStringAttribute ("name"));
            }
        }
    }

    // find a results file
    FileChooser outputChooser ("Select a place to save results ...", File::nonexistent, "*.mstr");

    if (outputChooser.browseForFileToOpen())
    {
        outputFile = outputChooser.getResult().withFileExtension (".mstr");

        if (outputFile.exists())
        {
            XmlDocument outputDoc (outputFile);
            outputElement = outputDoc.getDocumentElement();
        }
        else
        {
            outputElement = new XmlElement ("TEST");
        }
    }
    
    sessionElement = new XmlElement ("SESSION");
    outputElement->prependChildElement (sessionElement);
    
    // make a random order for the sliders and adjectives
    numSamples = audioFileSources.size();
    numAdjectives = adjectives.size();
    
    sliderOrder.setSize (numSamples);
    adjectiveOrder.setSize (numAdjectives);

    adjectiveIndex = 0;
    currentAdjective = adjectives [adjectiveOrder [adjectiveIndex]];

    // put all the sliders and buttons in place
    int height = 530;
    int width = numSamples * 120 + 20;

    setSize (width, height);

    for (int sampleNumber = 0; sampleNumber < numSamples; ++sampleNumber)
    {
        sampleButtons.add (new TextButton ("Sample" + String (sampleNumber + 1)));

        TextButton* button = sampleButtons [sampleNumber];
        addAndMakeVisible (button);
        button->addListener (this);

        int xPos = 20 + 120 * sampleNumber;
        button->setBounds (xPos, 400, 100, 40);

        sampleSliders.add (new Slider (Slider::LinearVertical, Slider::TextBoxBelow));

        Slider* slider = sampleSliders [sampleNumber];
        addAndMakeVisible (slider);
        slider->setBounds (xPos, 70, 100, 300);
        slider->setRange (0, 100, 1);
        slider->addListener (this);

        // some data to use later
        listenedSamples.add (false);
        currentResults.add (0);
    }

    addAndMakeVisible (&continueButton);
    continueButton.addListener (this);
    continueButton.setBounds (width / 2 - 50, 460, 100, 40);    
    continueButton.setEnabled (false);
    continueButton.setColour (TextButton::buttonColourId, Colours::yellow);

    addAndMakeVisible (&instructionText);
    instructionText.setText (String ("Grade the samples by how '") + currentAdjective + String ("' they are."), dontSendNotification);
    instructionText.setBounds (0, 0, width, 50);
    instructionText.setJustificationType (Justification::centred);
    instructionText.setFont (Font (22));

    // some audio setup
    deviceManager.initialise (0, 2, 0, true);
    deviceManager.addAudioCallback (&sourcePlayer);
    sourcePlayer.setSource (&transportSource);
}

MainContentComponent::~MainContentComponent()
{
    // crashing happens if we don't do this
    transportSource.setSource (nullptr);
    sourcePlayer.setSource (nullptr);
    deviceManager.removeAudioCallback (&sourcePlayer);
}

//==============================================================================
void MainContentComponent::paint (Graphics& g)
{
}

void MainContentComponent::resized()
{
}

//==============================================================================
void MainContentComponent::buttonClicked (Button* clickedButton)
{
    for (int buttonNumber = 0; buttonNumber < numSamples; ++buttonNumber)
    {
        if (clickedButton == sampleButtons [buttonNumber])
        {
            int sampleNumber = sliderOrder [buttonNumber];

            transportSource.stop();
            transportSource.setSource (nullptr);
            transportSource.setSource (audioFileSources [sampleNumber]);
            transportSource.start();

            listenedSamples.set (buttonNumber, true);

            if (! listenedSamples.contains (false))
            {
                continueButton.setEnabled (true);
            }
        }
    }

    if (clickedButton == &continueButton)
    {
        nextAdjective();
    }
}

//==============================================================================
void MainContentComponent::sliderValueChanged (Slider* movedSlider)
{
    for (int sliderNumber = 0; sliderNumber < numSamples; ++sliderNumber)
    {
        if (movedSlider == sampleSliders [sliderNumber])
        {
            currentResults.set (sliderOrder [sliderNumber], sampleSliders [sliderNumber]->getValue());
        }
    }
}

//==============================================================================
void MainContentComponent::nextAdjective()
{
    transportSource.stop();

    continueButton.setEnabled (false);

    resultElements.add (new XmlElement (currentAdjective));


    for (int i = 0; i < numSamples; ++i)
    {
        resultElements.getLast()->setAttribute ("Sample" + String (i + 1), currentResults [i]);
    }

    sessionElement->prependChildElement (resultElements.getLast());

    ++adjectiveIndex;

    if (adjectiveIndex < numAdjectives)
    {
        currentAdjective = adjectives [adjectiveOrder [adjectiveIndex]];
        instructionText.setText (String ("Grade the samples by how '") + currentAdjective + String ("' they are."), dontSendNotification);

        for (int k = 0; k < numSamples; ++k)
        {
            listenedSamples.set (k, false);
            // set to 100 first to make sure all sliders get updated
            sampleSliders [k]->setValue (100);
            sampleSliders [k]->setValue (0);
        }

        sliderOrder.shuffle();
    }
    else
    {
        for (int k = 0; k < numSamples; ++k)
        {
            sampleButtons [k]->setEnabled (false);
            sampleSliders [k]->setEnabled (false);
        }
    }
}

//==============================================================================
void MainContentComponent::writeResults()
{
    endTime = Time::getCurrentTime ();

    sessionElement->setAttribute ("StartTime", startTime.toString(true, true, true, true));
    sessionElement->setAttribute ("EndTime", endTime.toString(true, true, true, true));
    outputElement->writeToFile (outputFile, "");
}
