/*
  ==============================================================================

    RandomOrderArray.cpp
    Created: 20 Jan 2014 2:20:34pm
    Author:  id115387

  ==============================================================================
*/

#include "RandomOrderArray.h"

RandomOrderArray::RandomOrderArray()
{
}

RandomOrderArray::RandomOrderArray (int length)
    : range (0, length - 1)
{
    for (int i = 0; i < length; ++i)
    {
        enclosedArray.add (i);
    }

    shuffle();
}

RandomOrderArray::~RandomOrderArray()
{
}

void RandomOrderArray::setSize (int length)
{
    range.setStart (0);
    range.setEnd (length - 1);

    enclosedArray.clear();

    for (int i = 0; i < length; ++i)
    {
        enclosedArray.add (i);
    }

    shuffle();
}

int RandomOrderArray::operator[] (const int index)
{
    return enclosedArray [index];
}

void RandomOrderArray::shuffle()
{
    for (int k = 0; k < enclosedArray.size(); ++k)
    {
        int randomValue = random.nextInt (range);
        enclosedArray.swap (k, randomValue);
    }
}
