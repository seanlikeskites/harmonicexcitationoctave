/*
  ==============================================================================

    RandomOrderArray.h
    Created: 20 Jan 2014 2:20:34pm
    Author:  id115387

  ==============================================================================
*/

#ifndef RANDOMORDERARRAY_H_INCLUDED
#define RANDOMORDERARRAY_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class RandomOrderArray
{
public:
    RandomOrderArray ();
    RandomOrderArray (int length);
    ~RandomOrderArray();

    void setSize (int length);

    int operator[] (const int index);

    void shuffle();

private:
    Array <int> enclosedArray;
    Range <int> range;
    Random random;
};

#endif  // RANDOMORDERARRAY_H_INCLUDED
