/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "RandomOrderArray.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   : public Component,
                               public Button::Listener,
                               public Slider::Listener
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void paint (Graphics&);
    void resized();

    void writeResults();

private:
    //==============================================================================
    OwnedArray <TextButton> sampleButtons;
    OwnedArray <Slider> sampleSliders;
    TextButton continueButton;
    Label instructionText;

    RandomOrderArray sliderOrder;
    Array <bool> listenedSamples;
    Array <int> currentResults;

    int numSamples;
    int numAdjectives;

    RandomOrderArray adjectiveOrder;

    void buttonClicked (Button*);

    void sliderValueChanged (Slider*);

    StringArray adjectives;
    int adjectiveIndex;
    String currentAdjective;
    void nextAdjective();

    ScopedPointer <XmlElement> testInfo;
    
    File outputFile;
    ScopedPointer <XmlElement> outputElement;
    XmlElement* sessionElement;
    Array <XmlElement*> resultElements;

    Time startTime, endTime;

    AudioFormatManager formatManager;
    OwnedArray <AudioFormatReaderSource> audioFileSources;
    AudioDeviceManager deviceManager;
    AudioSourcePlayer sourcePlayer;
    AudioTransportSource transportSource;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
