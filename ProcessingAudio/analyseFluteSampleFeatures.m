fluteSampleFiles = dir('./VAMETest/LoudnessMatchedFluteSamples/*.wav');
numSamples = length(fluteSampleFiles);

for sample = 1:numSamples
	fluteSamples(sample).name = fluteSampleFiles(sample).name(1:end-4);
	tempFilePath = ['./VAMETest/LoudnessMatchedFluteSamples/' fluteSampleFiles(sample).name];

	[tempData tempFs] = wavread(tempFilePath);
	fluteSamples(sample).data = tempData;
	fluteSamples(sample).fs = tempFs;

	tempF0 = xtract_hps(fluteSamples(sample).data, fluteSamples(sample).fs);
	fluteSamples(sample).f0 = tempF0;

	fluteSamples(sample).inharmonicicty = xtract_spectral_inharmonicity(tempData, tempFs, tempF0);
	fluteSamples(sample).tristimulus1 = xtract_tristimulus(tempData, tempFs, 1, tempF0);
	fluteSamples(sample).tristimulus2 = xtract_tristimulus(tempData, tempFs, 2, tempF0);
	fluteSamples(sample).tristimulus3 = xtract_tristimulus(tempData, tempFs, 3, tempF0);
	fluteSamples(sample).parityRatio = xtract_odd_even_ratio(tempData, tempFs, tempF0);
	fluteSamples(sample).irregularityK = xtract_irregularity(tempData, 'k');
	fluteSamples(sample).irregularityJ = xtract_irregularity(tempData, 'j');
	fluteSamples(sample).centroid = xtract_spectral_centroid(tempData, tempFs);
	fluteSamples(sample).loudness = xtract_loudness(tempData, tempFs);
	fluteSamples(sample).noisiness = xtract_noisiness(tempData, tempFs, tempF0, 0.01);
	fluteSamples(sample).spectralSlope = xtract_spectral_slope(tempData, tempFs);
end
