%======================================================================================
% Clarinet Samples
%======================================================================================

[clarinet clarinetFs] = wavread('clarinet.wav');
clarinetArray = zeros(length(clarinet), 11);
clarinetArray(:, 1) = clarinet;
clarinetArray(:, 2) = wavread('clarinetSTFT50.wav');
clarinetArray(:, 3) = wavread('clarinetSTFT100.wav');
clarinetArray(:, 4) = wavread('clarinetSTFT500.wav');
clarinetArray(:, 5) = wavread('clarinetSSB50.wav');
clarinetArray(:, 6) = wavread('clarinetSSB100.wav');
clarinetArray(:, 7) = wavread('clarinetSSB500.wav');
clarinetArray(:, 8) = wavread('clarinetInstantaneous50.wav');
clarinetArray(:, 9) = wavread('clarinetInstantaneous100.wav');
clarinetArray(:, 10) = wavread('clarinetInstantaneous500.wav');
clarinetArray(:, 11) = wavread('clarinetFiltered.wav');

clarinetInharmonicities = zeros(1, 11);
for k = 1:11
	clarinetInharmonicities(k) = inharmonicity(clarinetArray(:, k), clarinetFs);
end

figure(1);
bar(clarinetInharmonicities);
title("Clarinet Samples");

%======================================================================================
% Piano Samples
%======================================================================================

[piano pianoFs] = wavread('piano.wav');
pianoArray = zeros(length(piano), 11);
pianoArray(:, 1) = piano;
pianoArray(:, 2) = wavread('pianoSTFT50.wav');
pianoArray(:, 3) = wavread('pianoSTFT100.wav');
pianoArray(:, 4) = wavread('pianoSTFT500.wav');
pianoArray(:, 5) = wavread('pianoSSB50.wav');
pianoArray(:, 6) = wavread('pianoSSB100.wav');
pianoArray(:, 7) = wavread('pianoSSB500.wav');
pianoArray(:, 8) = wavread('pianoInstantaneous50.wav');
pianoArray(:, 9) = wavread('pianoInstantaneous100.wav');
pianoArray(:, 10) = wavread('pianoInstantaneous500.wav');
pianoArray(:, 11) = wavread('pianoFiltered.wav');

pianoInharmonicities = zeros(1, 11);
for k = 1:11
	pianoInharmonicities(k) = inharmonicity(pianoArray(:, k), pianoFs);
end

figure(2);
bar(pianoInharmonicities);
title("Piano Sample");

%======================================================================================
% Synth Samples
%======================================================================================

[synth synthFs] = wavread('synth.wav');
synthArray = zeros(length(synth), 11);
synthArray(:, 1) = synth;
synthArray(:, 2) = wavread('synthSTFT50.wav');
synthArray(:, 3) = wavread('synthSTFT100.wav');
synthArray(:, 4) = wavread('synthSTFT500.wav');
synthArray(:, 5) = wavread('synthSSB50.wav');
synthArray(:, 6) = wavread('synthSSB100.wav');
synthArray(:, 7) = wavread('synthSSB500.wav');
synthArray(:, 8) = wavread('synthInstantaneous50.wav');
synthArray(:, 9) = wavread('synthInstantaneous100.wav');
synthArray(:, 10) = wavread('synthInstantaneous500.wav');
synthArray(:, 11) = wavread('synthFiltered.wav');

synthInharmonicities = zeros(1, 11);
for k = 1:11
	synthInharmonicities(k) = inharmonicity(synthArray(:, k), synthFs);
end

figure(3);
bar(synthInharmonicities);
title("Synth Samples");

%======================================================================================
% Bass Samples
%======================================================================================

[bass bassFs] = wavread('bass.wav');
bassArray = zeros(length(bass), 11);
bassArray(:, 1) = bass;
bassArray(:, 2) = wavread('bassSTFT50.wav');
bassArray(:, 3) = wavread('bassSTFT100.wav');
bassArray(:, 4) = wavread('bassSTFT500.wav');
bassArray(:, 5) = wavread('bassSSB50.wav');
bassArray(:, 6) = wavread('bassSSB100.wav');
bassArray(:, 7) = wavread('bassSSB500.wav');
bassArray(:, 8) = wavread('bassInstantaneous50.wav');
bassArray(:, 9) = wavread('bassInstantaneous100.wav');
bassArray(:, 10) = wavread('bassInstantaneous500.wav');
bassArray(:, 11) = wavread('bassFiltered.wav');

bassInharmonicities = zeros(1, 11);
for k = 1:11
	bassInharmonicities(k) = inharmonicity(bassArray(:, k), bassFs);
end

figure(4);
bar(bassInharmonicities);
title("Bass Samples");
