function out = matchSpectralContent(signalToProcess, fsProc, signalToMatch, fsMatch)
% A function which attempts to change the harmonic content of a signal to match that of another.
%
% Usage: out = matchSpectralContent(signalToProcess, fsProc, signalToMatch, fsMatch)
% 
% Return Values:
% (out) is the processed signal.
%
% Arguments:
% (signalToProcess) is the input signal to be altered.
% (fsProc) is the sample rate of that signal.
% (signalToMatch) is the signal to match the harmonic content of.
% (fsMatch) is the sample rate of that signal.

% get the amplitudes of harmonics in the signals
[ordersIn amplitudesIn f0In] = relativeHarmonicAmplitudes(signalToProcess, fsProc, false, true);
[ordersMatch amplitudesMatch] = relativeHarmonicAmplitudes(signalToMatch, fsMatch, false, true);

% manipulate some arrays to better analyse the spectral content of each signal
maximumHarmonic = max([ordersIn ordersMatch]);
fullAmplitudesIn = fullAmplitudesMatch = zeros(1, maximumHarmonic);
fullAmplitudesIn(ordersIn) = amplitudesIn;
fullAmplitudesMatch(ordersMatch) = amplitudesMatch;

% find out which harmonics need increasing in level
harmonicsToExcite = find(fullAmplitudesIn < fullAmplitudesMatch);

% generate the harmonics and add them to the signal
%workingSignal = signalToProcess;
%for harmonic = harmonicsToExcite
%	tempHarmonic = iap(signalToProcess, fsProc, f0In, 500, harmonic);
%	workingSignal += fullAmplitudesMatch(harmonic)*tempHarmonic;
%end
%
%out = workingSignal;

% generate the harmonics and add them to the signal
workingSignal = signalToProcess;

for harmonic = harmonicsToExcite
	amplitudeToMatch = fullAmplitudesMatch(harmonic);
	currentAmplitude = fullAmplitudesIn(harmonic);
	fullTempAmplitudes = zeros(1, maximumHarmonic);
	if amplitudeToMatch
		tempHarmonic = iap(signalToProcess, fsProc, f0In, harmonic, 500);
		loopCount = 0;
		while abs(amplitudeToMatch - currentAmplitude) > 0.01
			tempGain = amplitudeToMatch - currentAmplitude;
			workingSignal += tempGain*tempHarmonic;
			[tempOrders tempAmplitudes] = relativeHarmonicAmplitudes(workingSignal, fsProc, false, true);
			fullTempAmplitudes(tempOrders) = tempAmplitudes;
			currentAmplitude = fullTempAmplitudes(harmonic);
			if ++loopCount > 9
				disp(['Exited loop for harmonic ' num2str(harmonic) '!']);
				fflush(stdout);
				break;
			end
		end
	end
end

out = workingSignal;
