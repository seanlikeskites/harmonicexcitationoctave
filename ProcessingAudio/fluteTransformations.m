players = {'A', 'B', 'C'};
%================================================================
% CSharp4 is not a good example, player B does not win
%================================================================
notes = {'D4', 'E4', 'FSharp4', 'G4', 'A4', 'B4', 'CSharp5'};

processedSamples = cell(2, 13);

if ~exist('ProcessedSamples')
	mkdir('ProcessedSamples');
end

if ~exist('Graphs')
	mkdir('Graphs');
end

for noteIndex = 1:length(notes);
	
	note = notes{noteIndex};

	disp(note);
	fflush(stdout);
	
	tempStringA = ['FluteSamples/PlayerA' note '.wav'];
	[tempA tempFsA] = wavread(tempStringA);
	
	tempStringB = ['FluteSamples/PlayerB' note '.wav'];
	[tempB tempFsB] = wavread(tempStringB);

	tempStringC = ['FluteSamples/PlayerC' note '.wav'];
	[tempC tempFsC] = wavread(tempStringC);

	tempAToB = matchSpectralContent(tempA, tempFsA, tempB, tempFsB);
	processedSamples{1, noteIndex} = tempAToB;
	tempWriteA = ['ProcessedSamples/' note 'AToB.wav'];
	wavwrite(tempAToB, tempFsA, tempWriteA);

	tempCToB = matchSpectralContent(tempC, tempFsC, tempB, tempFsB);
	processedSamples{2, noteIndex} = tempCToB;
	tempWriteC = ['ProcessedSamples/' note 'CToB.wav'];
	wavwrite(tempCToB, tempFsC, tempWriteC);

	[tempOrdersA tempAmplitudesA] = relativeHarmonicAmplitudes(tempA, tempFsA, false, true);
	[tempOrdersB tempAmplitudesB] = relativeHarmonicAmplitudes(tempB, tempFsB, false, true);
	[tempOrdersC tempAmplitudesC] = relativeHarmonicAmplitudes(tempC, tempFsC, false, true);
	[tempOrdersAToB tempAmplitudesAToB] = relativeHarmonicAmplitudes(tempAToB, tempFsA, false, true);
	[tempOrdersCToB tempAmplitudesCToB] = relativeHarmonicAmplitudes(tempCToB, tempFsC, false, true);
	
	tempMaxOrder = max([tempOrdersA tempOrdersB tempOrdersC tempOrdersAToB tempOrdersCToB]);
	tempFullAmplitudes = zeros(5, tempMaxOrder);
	tempFullAmplitudes(1, tempOrdersB) = tempAmplitudesB;
	tempFullAmplitudes(2, tempOrdersA) = tempAmplitudesA;
	tempFullAmplitudes(3, tempOrdersAToB) = tempAmplitudesAToB;
	tempFullAmplitudes(4, tempOrdersC) = tempAmplitudesC;
	tempFullAmplitudes(5, tempOrdersCToB) = tempAmplitudesCToB;

	tempFig = figure(noteIndex);
	set(tempFig, 'visible', 'off');
	bar(tempFullAmplitudes);
	set(gca, 'xtick', [1 2 3 4 5]);
	set(gca, 'xticklabel', {'B' 'A' 'AToB' 'C' 'CToB'});
	title(note);
	xlabel('Sample');
	ylabel('Amplitude');
	legendEntries = cell(1, tempMaxOrder);
	for i = 1:tempMaxOrder
		legendEntries{i} = ['f' num2str(i - 1)];
	end
	legend(legendEntries);
	
	print(['Graphs/' note 'Transformations.eps'], '-deps', '-color');
end
