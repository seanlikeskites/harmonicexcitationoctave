function VAMETrainingCallbacks(varargin)
% Callback functions for the collection of extra adjectives
	functionName = varargin{1};
	feval(functionName, varargin{2:end});
end

function playSample(sampleNumber)
	global audioToPlay preSamples buttonMatrix playedMatrix;
	stop(audioToPlay);
	audioData = preSamples{sampleNumber, 1};
	audioFs = preSamples{sampleNumber, 2};
	audioToPlay = audioplayer(audioData, audioFs);

	global lastPlayed;
	selectedColour = [0 150 0]/255;
	if lastPlayed
		set(buttonMatrix(lastPlayed), 'BackgroundColor', selectedColour');
	end

	playingColour = [30 144 255]/255;
	set(buttonMatrix(sampleNumber), 'BackgroundColor', playingColour);

	global continueButton;
	playedMatrix(sampleNumber) = 1;
	if prod(playedMatrix)
		set(continueButton, 'Enable', 'on');
	end

	lastPlayed = sampleNumber;
	play(audioToPlay);
end

function stopAudio()
	global audioToPlay;
	stop(audioToPlay);
end

function closeWindow()
	stopAudio();
	clear global;
	closereq;
end

function proceed()
	stopAudio();
	closereq;
	VAMEGrading(1);
end

