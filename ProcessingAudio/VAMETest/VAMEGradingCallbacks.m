function VAMEGradingCallbacks(varargin)
% Callback functions for the sample grading stage
	functionName = varargin{1};
	feval(functionName, varargin{2:end});
end

% run when slider value is updated
function sliderMoved(sliderNumber)
    global sliderMatrix textBoxMatrix;
    slider = sliderMatrix(sliderNumber);
    newValue = fix(get(slider, 'Value'));
    set(slider, 'Value', newValue);
    textBox = textBoxMatrix(sliderNumber);
    set(textBox, 'String', num2str(newValue));
end

function playSample()
	global audioToPlay;
	stop(audioToPlay);
	tempStruct.keepPlaying = 1;
	audioToPlay.UserData = tempStruct;
	play(audioToPlay);
end

function stopAudio()
	global audioToPlay;
	tempStruct.keepPlaying = 0;
	audioToPlay.UserData = tempStruct;
	stop(audioToPlay);
end

function closeWindow()
	stopAudio();
	clear global;
	closereq;
end

function proceed(experimentNumber)
	global samples sampleNumber adjectives sliderMatrix;
	numSamples = size(samples, 1);
	numAdjectives = length(adjectives);

	experimentResults = zeros(1, numAdjectives);
	for adjectiveNumber = 1:numAdjectives
		experimentResults(adjectiveNumber) = get(sliderMatrix(adjectiveNumber), 'Value');
	end

	stopAudio();
	closereq;
	
	global resultsFileLocation;
	load (resultsFileLocation, '-mat');
	results.resultArray(sampleNumber, :) = experimentResults;
	results.experimentNumber = results.experimentNumber + 1;
	save(resultsFileLocation, 'results');

	if experimentNumber < numSamples
		VAMEGrading(experimentNumber + 1);
	else
		msgbox('Thank You For Participating, Have a Nice Day Now!', 'Test Complete');
		clear global;
	end
end
