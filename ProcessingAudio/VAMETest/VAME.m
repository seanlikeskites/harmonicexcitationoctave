function VAME(config)
% Start a VAME test

if ~exist(config, 'file')
    errordlg('The configuration file you have requested does not exist.');
    clear global;
    return;
end
file = fopen(config, 'r');

[configFilePath configFileName configFileExtension] = fileparts(config);

% parse the file
preSampleStrings = cell(0);
sampleStrings = cell(0);
global adjectives;
adjectives = cell(0);
block = 0;
lineInBlock = 0;
while true
	currentLine = fgetl(file);
	if currentLine == -1
		break;
	end

	if prod(double(isspace(currentLine)))
		if block > 3
			break;
		end
		lineInBlock = 0;
		continue;
	else
		lineInBlock = lineInBlock + 1;
	end

	if lineInBlock == 1
		block = block + 1;
	end

	switch block
		case 1
			preSampleStrings = [preSampleStrings currentLine];
		case 2
			sampleStrings = [sampleStrings currentLine];
		case 3
			adjectives = [adjectives currentLine];
	end
end

fclose(file);

numPreSamples = length(preSampleStrings);
numSamples = length(sampleStrings);
numAdjectives = length(adjectives);

% check that all samples are valid audio files then read into samples array
global preSamples;
preSamples = cell(numPreSamples, 2);
for experimentNumber = 1:numPreSamples
	if strcmp(wavfinfo(preSampleStrings{experimentNumber}), '')
	    errordlg(['Preliminary Sample Number ' num2str(experimentNumber) ' Is Not A Valid Wave File']);
	    return;
	else
	    [preSamples{experimentNumber, 1}, preSamples{experimentNumber, 2}] = wavread(preSampleStrings{experimentNumber});
	end
end

global samples;
samples = cell(numSamples, 2);
for experimentNumber = 1:numSamples
	if strcmp(wavfinfo(sampleStrings{experimentNumber}), '')
	    errordlg(['Sample Number ' num2str(experimentNumber) ' Is Not A Valid Wave File']);
	    return;
	else
		[tempAudioData tempFs] = wavread(sampleStrings{experimentNumber});
		tempAudioData = [tempAudioData; zeros(tempFs/2, size(tempAudioData, 2))];
		samples{experimentNumber, 1} = tempAudioData;
		samples{experimentNumber, 2} = tempFs;
%	    [samples{experimentNumber, 1}, samples{experimentNumber, 2}] = wavread(sampleStrings{experimentNumber});
	end
end

% save file stuff
if ~isequal(configFilePath, '')
    configFilePath = [configFilePath '\'];
end

% ask subject for a save file location
global resultsFileLocation;

[resultsFileName, resultsFilePath] = uiputfile([configFilePath 'MyResults.vam'], 'Name Results File');
resultsFileLocation = [resultsFilePath resultsFileName];

% save to a file
if resultsFileName == 0
	clear global;
	return;
else
	results = VAMEResult(numSamples, numAdjectives);
	results.experimentNumber = 1;
	save(resultsFileLocation, 'results');
end

VAMETraining();
%VAMEGrading(1);

