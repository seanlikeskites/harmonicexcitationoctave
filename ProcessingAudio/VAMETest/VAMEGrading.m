function VAMEGrading(experimentNumber)
% GUI for the grading stage

% get adjectives and samples
global adjectives samples;
numAdjectives = length(adjectives);
numSamples = size(samples, 1);

% set the audio player up
global audioToPlay resultsFileLocation sampleNumber;
load(resultsFileLocation, '-mat');
sampleNumber = results.sampleOrder(experimentNumber);
audioData = samples{sampleNumber, 1};
audioFs = samples{sampleNumber, 2};
audioToPlay = audioplayer(audioData, audioFs);
tempStruct.keepPlaying = 1;
audioToPlay.UserData = tempStruct;
audioToPlay.StopFcn = @loopSample;

% sizes of stuff
sliderWidth = 20;
sliderHeight = 200;
buttonWidth = 100;
buttonHeight = 30;
textBoxWidth = 50;
textBoxHeight = 20;
verticalSeparation = 20;
horizontalSeparation = 20;
labelHeight = 20;
labelWidth = 100;

% define window dimensions
figHeight = sliderHeight + 2*buttonHeight + 4*verticalSeparation + 30 + textBoxHeight + 2*labelHeight;
figWidth = numAdjectives*(labelWidth + horizontalSeparation) + horizontalSeparation;
minWidth = 3*(buttonWidth + horizontalSeparation) + horizontalSeparation;
if figWidth < minWidth
	figWidth = minWidth;
end

% get screen info and hence window positions
screenSize = get(0, 'ScreenSize');
screenWidth = screenSize(3);
screenHeight = screenSize(4);
windowX = screenWidth/2 - figWidth/2;
windowY = screenHeight/2 - figHeight/2;

% create window
windowColour = [102 139 139]/255;
fig = figure('Position', [windowX, windowY, figWidth, figHeight], 'ToolBar', 'none', 'MenuBar', 'none', 'Name', 'VAME - Sample Grading', 'NumberTitle', 'off', 'Resize', 'off', 'Color', windowColour, 'CloseRequestFcn', 'VAMEGradingCallbacks(''closeWindow'')');

% add a title
yPosition = figHeight - verticalSeparation - buttonHeight;
uicontrol(fig, 'Style', 'text', 'Position', [horizontalSeparation, yPosition, figWidth - horizontalSeparation, buttonHeight], 'String', ['Sample ' num2str(experimentNumber) ': Grade the sample on each of the scales'], 'BackgroundColor', windowColour, 'FontSize', 14, 'FontWeight', 'bold');

% add sliders
global sliderMatrix textBoxMatrix;
sliderMatrix = zeros(1, numAdjectives);
textBoxMatrix = zeros(1, numAdjectives);
yPositionTopLabel = yPosition - verticalSeparation - labelHeight;
yPositionSlider = yPositionTopLabel - 10 - sliderHeight;
yPositionBottomLabel = yPositionSlider - 10 - labelHeight;
yPositionTextBox = yPositionBottomLabel - textBoxHeight - 10;
for sliderNumber = 1:numAdjectives
	xPositionLabel = (sliderNumber -1)*(labelWidth + horizontalSeparation) + horizontalSeparation;
	xPositionSlider = xPositionLabel + (labelWidth - sliderWidth)/2;
	xPositionTextBox = xPositionLabel + (labelWidth - textBoxWidth)/2;
	uicontrol(fig, 'Style', 'text', 'Position', [xPositionLabel, yPositionTopLabel, labelWidth, labelHeight], 'String', ['More ' adjectives{sliderNumber}], 'FontSize', 10', 'FontWeight', 'bold', 'BackgroundColor', windowColour);
	sliderMatrix(sliderNumber) = uicontrol(fig, 'Style', 'slider', 'Position', [xPositionSlider, yPositionSlider, sliderWidth, sliderHeight], 'Min', 0, 'Max', 100, 'SliderStep', [0.1, 0.01], 'CallBack', ['VAMEGradingCallbacks(''sliderMoved'',' num2str(sliderNumber) ')']);
	uicontrol(fig, 'Style', 'text', 'Position', [xPositionLabel, yPositionBottomLabel, labelWidth, labelHeight], 'String', ['Less ' adjectives{sliderNumber}], 'FontSize', 10', 'FontWeight', 'bold', 'BackgroundColor', windowColour);
	textBoxMatrix(sliderNumber) = uicontrol(fig, 'Style', 'text', 'Position', [xPositionTextBox, yPositionTextBox, textBoxWidth, textBoxHeight], 'FontSize', 12, 'BackgroundColor', windowColour, 'String', 0);
end

% add other buttons
xPosition = figWidth/2 - horizontalSeparation - 1.5*buttonWidth;
playButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Play Sample', 'CallBack', 'VAMEGradingCallbacks(''playSample'')');

xPosition = xPosition + horizontalSeparation + buttonWidth;
stopButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Stop Audio', 'CallBack', 'VAMEGradingCallbacks(''stopAudio'')');

xPosition = xPosition + horizontalSeparation + buttonWidth;
continueButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Continue', 'CallBack', ['VAMEGradingCallbacks(''proceed'',' num2str(experimentNumber) ')']);

% pretty lines
canvas = axes('Units', 'pixels', 'Position', [0, 0, figWidth, figHeight], 'Visible', 'off', 'Xlim', [0, figWidth], 'Ylim', [0, figHeight]);

lineSeparations = [buttonHeight + 1.5*verticalSeparation,
		   labelHeight + 15,
		   sliderHeight + 10,
		   labelHeight + 10,
		   textBoxHeight + 10];
yPosition = figHeight; 
for lineNumber = 1:length(lineSeparations)
	yPosition = yPosition - lineSeparations(lineNumber);
	line(10:figWidth - 10, yPosition, 'Parent', canvas, 'Color', 'black');
end

lineTop = figHeight - buttonHeight - 1.5*verticalSeparation;
lineBottom = buttonHeight + 1.5*verticalSeparation + 5;
xStep = labelWidth + horizontalSeparation;
for xPosition = 10:xStep:figWidth - 10
	line(xPosition, lineBottom:lineTop, 'Parent', canvas, 'Color', 'black');
end
