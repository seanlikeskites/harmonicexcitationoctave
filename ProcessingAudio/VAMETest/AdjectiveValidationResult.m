classdef AdjectiveValidationResult
	% a class to contain the results from an adjective validation test

	properties
		resultArray
		sampleOrder
		experimentNumber
		extraAdjectives
	end

	methods
		function obj = AdjectiveValidationResult(numSamples, numAdjectives);
			obj.resultArray = zeros(numSamples, numAdjectives);
			obj.sampleOrder = randperm(numSamples);
			obj.experimentNumber = 0;
		end
		
	end

end

