fileNames = dir('*.vam');
numFiles = length(fileNames);

for k = 1:numFiles
    name = fileNames(k).name;
    [path id ext] = fileparts(name);
    load(name, '-mat');
    tempResults = results.resultArray;
    newName = [id '.tvam'];
    save(newName, '-ascii', 'tempResults');
end