function ExtraAdjectives()
% GUI for collecting extra adjectives

global samples;
numSamples = size(samples, 1);

% Work out grid sizes
numColumns = 6;
numRows = ceil(numSamples/numColumns);

% button sizes
buttonWidth = 100;
buttonHeight = 30;
horizontalSeparation = 20;
verticalSeparation = 20;

% set window dimensions
figWidth = numColumns*(buttonWidth + horizontalSeparation) + horizontalSeparation;
figHeight = (numRows + 2)*(buttonHeight + verticalSeparation) + 2*verticalSeparation + 200;

% get screen info and hence window positions
screenSize = get(0, 'ScreenSize');
screenWidth = screenSize(3);
screenHeight = screenSize(4);
windowX = screenWidth/2 - figWidth/2;
windowY = screenHeight/2 - figHeight/2;

% create window
windowColour = [102 139 139]/255;
fig = figure('Position', [windowX, windowY, figWidth, figHeight], 'ToolBar', 'none', 'MenuBar', 'none', 'Name', 'VAME - Extra Adjectives', 'NumberTitle', 'off', 'Resize', 'off', 'Color', windowColour, 'CloseRequestFcn', 'ExtraAdjectivesCallbacks(''closeWindow'')');

% add a title
yPosition = figHeight - verticalSeparation - buttonHeight;
uicontrol(fig, 'Style', 'text', 'Position', [horizontalSeparation, yPosition, figWidth - horizontalSeparation, buttonHeight], 'String', ['Suggest any extra adjectives to describe these samples.'], 'BackgroundColor', windowColour, 'FontSize', 14, 'FontWeight', 'bold');

% populate the window with buttons
global buttonMatrix lastPlayed;
buttonMatrix = zeros(1, numSamples);
lastPlayed = 0;

global resultsFileLocation sampleOrder;
load(resultsFileLocation, '-mat');
sampleOrder = results.sampleOrder;

nonSelectedColour = [172 23 31]/255;
selectedColour = [0 150 0]/255;

for sampleNumber = 1:numSamples
	rowNumber = ceil(sampleNumber/numColumns) + 1;
	yPosition = figHeight - (rowNumber*(buttonHeight + verticalSeparation));
	
	columnNumber = mod(sampleNumber - 1, numColumns);
	xPosition = columnNumber*(buttonWidth + horizontalSeparation) + horizontalSeparation;

	buttonMatrix(sampleNumber) = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, yPosition, buttonWidth, buttonHeight], 'String', ['Play Sample ' num2str(sampleNumber)], 'BackgroundColor', selectedColour, 'CallBack', ['ExtraAdjectivesCallbacks(''playSample'',' num2str(sampleNumber) ')']);
end

global textBox;
yPosition = buttonHeight + 2*verticalSeparation;
textBoxWidth = figWidth - 2*horizontalSeparation;
textBox = uicontrol(fig, 'Style', 'edit',  'Position', [horizontalSeparation, yPosition, textBoxWidth, 200], 'Min', 1, 'Max', 20, 'BackgroundColor', 'white', 'String', '', 'FontSize', 14);

xPosition = (figWidth - horizontalSeparation)/2 - buttonWidth;
stopButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Stop Audio', 'CallBack', 'ExtraAdjectivesCallbacks(''stopAudio'')');

xPosition = xPosition + horizontalSeparation + buttonWidth;
continueButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Continue', 'CallBack', 'ExtraAdjectivesCallbacks(''proceed'')');

