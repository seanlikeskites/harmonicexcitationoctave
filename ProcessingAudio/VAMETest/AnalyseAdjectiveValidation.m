fileNames = dir('Results/AdjectiveValidation/*.adj');
numFiles = length(fileNames);

masterResults = zeros(10, 62, numFiles);
masterExtraAdjectives = {};

for k = 1:numFiles
	load(['Results/AdjectiveValidation/' fileNames(k).name], '-mat');
	masterResults(:, :, k) = results.resultArray;
    if isempty(results.extraAdjectives)
        results.extraAdjectives = {0};
    end
    masterExtraAdjectives = [masterExtraAdjectives results.extraAdjectives];
end

file = fopen('FluteAdjectiveValidation.conf', 'r');

% parse the file
sampleStrings = cell(0);
global adjectives;
adjectives = cell(0);
block = 0;
lineInBlock = 0;
while true
	currentLine = fgetl(file);
	if currentLine == -1
		break
	end

	if prod(double(isspace(currentLine)))
		if block > 2
			break;
		end
		lineInBlock = 0;
		continue;
	else
		lineInBlock = lineInBlock + 1;
	end

	if lineInBlock == 1
		block = block + 1;
	end

	switch block
		case 1
			sampleStrings = [sampleStrings currentLine];
		case 2
			adjectives = [adjectives currentLine];
	end
end

fclose(file);

numSamples = length(sampleStrings);
numAdjectives = length(adjectives);

selectionCountAcrossSubjects = sum(masterResults, 3);
selectionCountAcrossSubjectsThenSamples = sum(selectionCountAcrossSubjects, 1);

[descendingPopularity, adjectivePopularityIndex] = sort(selectionCountAcrossSubjectsThenSamples, 'descend');

orderedAdjectives = adjectives(adjectivePopularityIndex);

selectionCountAcrossSamples = sum(masterResults, 1);
thresholdSelectionCountAcrossSamples = selectionCountAcrossSamples >= 4;
thresholdSelectionCountAcrossSamplesThenSubjects = sum(thresholdSelectionCountAcrossSamples, 3);

[thresholdDescendingPopularity, thresholdAdjectivePopularityIndex] = sort(thresholdSelectionCountAcrossSamplesThenSubjects, 'descend');

thresholdOrderedAdjectives = adjectives(thresholdAdjectivePopularityIndex);

for k = 1:62
	disp([thresholdOrderedAdjectives{k} ': ' num2str(thresholdDescendingPopularity(k))]);
end
