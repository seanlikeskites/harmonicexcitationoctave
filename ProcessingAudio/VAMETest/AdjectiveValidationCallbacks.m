function AdjectiveValidationCallbacks(varargin)
% Callback functions for the adjective validation process
	functionName = varargin{1};
	feval(functionName, varargin{2:end});
end

function toggleAdjectiveSelection(adjectiveNumber)
	global adjectiveSelected buttonMatrix;
	adjectiveSelected(adjectiveNumber) = 1 - adjectiveSelected(adjectiveNumber);

	nonSelectedColour = [102 139 139]/255;
	selectedColour = [0 150 0]/255;
	if adjectiveSelected(adjectiveNumber) == 0
		set(buttonMatrix(adjectiveNumber), 'BackgroundColor', nonSelectedColour);
	else
		set(buttonMatrix(adjectiveNumber), 'BackgroundColor', selectedColour);
	end
end

function playSample()
	global audioToPlay;
	stop(audioToPlay);
	play(audioToPlay);
end

function stopAudio()
	global audioToPlay;
	stop(audioToPlay);
end

function closeWindow()
	stopAudio();
	clear global;
	closereq;
end

function proceed(experimentNumber)
	global samples sampleNumber adjectiveSelected;
	numSamples = size(samples, 1);

	stopAudio();
	closereq;
	
	global resultsFileLocation;
	load (resultsFileLocation, '-mat');
	results.resultArray(sampleNumber, :) = adjectiveSelected;
	results.experimentNumber = results.experimentNumber + 1;
	save(resultsFileLocation, 'results');

	if experimentNumber < numSamples
		AdjectiveValidation(experimentNumber + 1);
	else
		ExtraAdjectives();
	end
end


%function proceed(experimentNumber)
%	global samples sampleNumber adjectives sliderMatrix;
%	numSamples = length(samples);
%	numAdjectives = length(adjectives);
%
%	experimentResults = zeros(1, numAdjectives);
%	for adjectiveNumber = 1:numAdjectives
%		experimentResults(adjectiveNumber) = get(sliderMatrix(adjectiveNumber), 'Value');
%	end
%
%	stopAudio();
%	closereq;
%	
%	global resultsFileLocation;
%	load (resultsFileLocation, '-mat');
%	results.resultArray(sampleNumber, :) = experimentResults;
%	results.experimentNumber = results.experimentNumber + 1;
%	save(resultsFileLocation, 'results');
%
%	if experimentNumber < numSamples
%		VAMEGrading(experimentNumber + 1);
%	else
%		msgbox('Thank You For Participating, Have a Nice Day Now!', 'Test Complete');
%		clear global;
%	end
%end
