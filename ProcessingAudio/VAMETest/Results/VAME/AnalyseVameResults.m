fileNames = dir('*.tvam');
numFiles = length(fileNames);

masterResults = zeros(25, 6, numFiles);

for k = 1:numFiles
	name = fileNames(k).name;
	tempResults = load(name);
	masterResults(:, :, k) = tempResults;
end

minResults = min(masterResults);
rangeResults = range(masterResults);

offsetResults = bsxfun(@minus, masterResults, minResults);
normalisedResults = bsxfun(@rdivide, offsetResults, rangeResults);

transposedResults = permute(masterResults, [1, 3, 2]);
transposedNormalisedResults = permute(normalisedResults, [1, 3, 2]);

reliabilities = zeros(1, 6);
reliabilitiesNormalised = zeros(1, 6);
for k = 1:6
	reliabilities(k) = AlphaReliability(transposedResults(:, :, k));
	reliabilitiesNormalised(k) = AlphaReliability(transposedNormalisedResults(:, :, k));
end

meanResults = mean(masterResults, 3);
meanNormalisedResults = mean(normalisedResults, 3);

reliableMeans = meanResults(:, reliabilities > 0.7);


names = {"AB4" "BB4" "CB4" "ABB4" "CBB4" "AG4" "BG4" "CG4" "ABG4" "CBG4" "AA4" "BA4" "CA4" "ABA4" "CBA4" "AD4" "BD4" "CD4" "ABD4" "CBD4" "AF4" "BF4" "CF4" "ABF4" "CBF4"};

adjectives = {"Clear" "Soft" "Round" "Gentle" "Smooth" "Reedy"};
reliableAdjectives = adjectives(reliabilities > 0.7);

save ReliableResults.ovam reliableMeans meanResults reliableAdjectives adjectives names;
