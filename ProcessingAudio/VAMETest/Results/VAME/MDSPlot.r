library(foreign)
data <- read.octave("ReliableResults.ovam")

values <- data$reliableMeans
names <- data$names
adjectives <- data$reliableAdjectives

distances <- dist(values)
scales <- cmdscale(distances, k=2)

xs <- scales[,1]
ys <- scales[,2]

colours <- c("blue", "red", "green", "tomato4", "black")

plot(xs, ys, type='n', main="Flute Sample Map", xlab="-Round and -Smooth", ylab="-Clear")
text(xs, ys, labels=names, col=colours)

for (i in 0:4)
{
	plot(xs, ys, type='n', main="Flute Sample Map", xlab="-Round and -Smooth", ylab="-Clear")
	text(xs[(5*i+1):(5*(i+1))], ys[(5*i+1):(5*(i+1))], labels=names[(5*i+1):(5*(i+1))], col=colours)
}

AYs <- ys[seq(1, 25, 5)]
BYs <- ys[seq(2, 25, 5)]
CYs <- ys[seq(3, 25, 5)]
ABYs <- ys[seq(4, 25, 5)]
CBYs <- ys[seq(5, 25, 5)]

order <- c(4, 5, 2, 3, 1)

notes <- c("D4", "F4", "G4", "A4", "B4")

barplot(AYs[order], names.arg=notes)
barplot(BYs[order], names.arg=notes)
barplot(CYs[order], names.arg=notes)
barplot(ABYs[order], names.arg=notes)
barplot(CBYs[order], names.arg=notes)

dev.off()
