function VAMETraining()
% GUI for collecting extra adjectives

global preSamples adjectives;
numSamples = size(preSamples, 1);

% Work out grid sizes
numColumns = 6;
numRows = ceil(numSamples/numColumns);

% button sizes
buttonWidth = 100;
buttonHeight = 30;
horizontalSeparation = 20;
verticalSeparation = 20;

% set window dimensions
figWidth = numColumns*(buttonWidth + horizontalSeparation) + horizontalSeparation;
figHeight = (numRows + 3)*(buttonHeight + verticalSeparation) + verticalSeparation;

% get screen info and hence window positions
screenSize = get(0, 'ScreenSize');
screenWidth = screenSize(3);
screenHeight = screenSize(4);
windowX = screenWidth/2 - figWidth/2;
windowY = screenHeight/2 - figHeight/2;

% create window
windowColour = [102 139 139]/255;
fig = figure('Position', [windowX, windowY, figWidth, figHeight], 'ToolBar', 'none', 'MenuBar', 'none', 'Name', 'VAME - Training Phase', 'NumberTitle', 'off', 'Resize', 'off', 'Color', windowColour, 'CloseRequestFcn', 'VAMETrainingCallbacks(''closeWindow'')');

% add a title
yPosition = figHeight - verticalSeparation - buttonHeight;
uicontrol(fig, 'Style', 'text', 'Position', [horizontalSeparation, yPosition, figWidth - horizontalSeparation, buttonHeight], 'String', 'Consider how these adjectives apply to the audio samples:', 'BackgroundColor', windowColour, 'FontSize', 14, 'FontWeight', 'bold');

% adjectives text
numAdjectives = length(adjectives);
allAdjectives = adjectives{1};
for adjectiveNumber = 2:numAdjectives
	allAdjectives = [allAdjectives ', ' adjectives{adjectiveNumber}];
end
yPosition = figHeight - 2*(verticalSeparation + buttonHeight);
uicontrol(fig, 'Style', 'text', 'Position', [horizontalSeparation, yPosition, figWidth - horizontalSeparation, buttonHeight], 'String', allAdjectives, 'BackgroundColor', windowColour, 'FontSize', 12);

% populate the window with buttons
global buttonMatrix lastPlayed playedMatrix;
buttonMatrix = zeros(1, numSamples);
playedMatrix = zeros(1, numSamples);
lastPlayed = 0;

nonPlayedColour = [172 23 31]/255;
nonSelectedColour = [172 23 31]/255;
selectedColour = [0 150 0]/255;

for sampleNumber = 1:numSamples
	rowNumber = ceil(sampleNumber/numColumns) + 1;
	yPosition = figHeight - ((rowNumber + 1)*(buttonHeight + verticalSeparation));
	
	columnNumber = mod(sampleNumber - 1, numColumns);
	xPosition = columnNumber*(buttonWidth + horizontalSeparation) + horizontalSeparation;

	buttonMatrix(sampleNumber) = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, yPosition, buttonWidth, buttonHeight], 'String', ['Play Sample ' num2str(sampleNumber)], 'BackgroundColor', nonPlayedColour, 'CallBack', ['VAMETrainingCallbacks(''playSample'',' num2str(sampleNumber) ')']);
end

xPosition = (figWidth - horizontalSeparation)/2 - buttonWidth;
stopButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Stop Audio', 'CallBack', 'VAMETrainingCallbacks(''stopAudio'')');

xPosition = xPosition + horizontalSeparation + buttonWidth;
global continueButton;
continueButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Continue', 'CallBack', 'VAMETrainingCallbacks(''proceed'')', 'Enable', 'off');

global audioToPlay;
audioToPlay = audioplayer(0, 44100);
