function ExtraAdjectivesCallbacks(varargin)
% Callback functions for the collection of extra adjectives
	functionName = varargin{1};
	feval(functionName, varargin{2:end});
end

function playSample(sampleNumber)
	global audioToPlay samples sampleOrder buttonMatrix;
	stop(audioToPlay);
	newSampleNumber = sampleOrder(sampleNumber);
	audioData = samples{newSampleNumber, 1};
	audioFs = samples{newSampleNumber, 2};
	audioToPlay = audioplayer(audioData, audioFs);

	global lastPlayed;
	selectedColour = [0 150 0]/255;
	if lastPlayed
		set(buttonMatrix(lastPlayed), 'BackgroundColor', selectedColour');
	end

	playingColour = [30 144 255]/255;
	set(buttonMatrix(sampleNumber), 'BackgroundColor', playingColour);

	lastPlayed = sampleNumber;
	play(audioToPlay);
end

function stopAudio()
	global audioToPlay;
	stop(audioToPlay);
end

function closeWindow()
	stopAudio();
	clear global;
	closereq;
end

function proceed()
	global textBox resultsFileLocation;
	load(resultsFileLocation, '-mat');
	results.extraAdjectives = get(textBox, 'String');
	save(resultsFileLocation, 'results');
	closeWindow();
	msgbox('Thank You For Participating, Have a Nice Day Now!', 'Test Complete');
end
