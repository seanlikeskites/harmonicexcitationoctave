classdef VAMEResult
	% a class to contain the results from a VAME test

	properties
		resultArray
		sampleOrder
		experimentNumber
	end

	methods
		function obj = VAMEResult(numSamples, numAdjectives);
			obj.resultArray = zeros(numSamples, numAdjectives);
			obj.sampleOrder = randperm(numSamples);
			obj.experimentNumber = 0;
		end
		
	end

end
