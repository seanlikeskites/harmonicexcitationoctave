function AdjectiveValidation(experimentNumber)
% GUI for the adjective validation process

%% Temporarily set some samples and adjectives
%adjectives = {'Bright', 'Shrill', 'Synthetic', 'Warm', 'Mellow', 'Heavy', 'Nasal', 'Rich', 'Round', 'Fused', 'Full', 'Crisp', 'Ringing'};
%fs = 96000;
%ts = 1/fs;
%t = ts:ts:5;
%sig = sin(2*pi*200*t);
%numSamples = 5;
%samples = cell(numSamples, 2);
%for k = 1:numSamples
%	samples{k, 1} = sig;
%	samples{k, 2} = fs;
%end
%numAdjectives = length(adjectives);
%
%% set the audio player up
%global audioToPlay;
%audioData = samples{experimentNumber, 1};
%audioFs = samples{experimentNumber, 2};
%audioToPlay = audioplayer(audioData, audioFs);

% get adjectives and samples
global adjectives samples;
numAdjectives = length(adjectives);
numSamples = size(samples, 1);

% set the audio player up
global audioToPlay resultsFileLocation sampleNumber;
load(resultsFileLocation, '-mat');
sampleNumber = results.sampleOrder(experimentNumber);
audioData = samples{sampleNumber, 1};
audioFs = samples{sampleNumber, 2};
audioToPlay = audioplayer(audioData, audioFs);

% Work out grid sizes
numColumns = 6;
numRows = ceil(numAdjectives/numColumns);

% button sizes
buttonWidth = 100;
buttonHeight = 30;
horizontalSeparation = 20;
verticalSeparation = 20;

% set window dimensions
figWidth = numColumns*(buttonWidth + horizontalSeparation) + horizontalSeparation;
figHeight = (numRows + 2)*(buttonHeight + verticalSeparation) + verticalSeparation;

% get screen info and hence window positions
screenSize = get(0, 'ScreenSize');
screenWidth = screenSize(3);
screenHeight = screenSize(4);
windowX = screenWidth/2 - figWidth/2;
windowY = screenHeight/2 - figHeight/2;

% create window
windowColour = [102 139 139]/255;
fig = figure('Position', [windowX, windowY, figWidth, figHeight], 'ToolBar', 'none', 'MenuBar', 'none', 'Name', 'VAME - Adjective Validation', 'NumberTitle', 'off', 'Resize', 'off', 'Color', windowColour, 'CloseRequestFcn', 'AdjectiveValidationCallbacks(''closeWindow'')');

% add a title
yPosition = figHeight - verticalSeparation - buttonHeight;
uicontrol(fig, 'Style', 'text', 'Position', [horizontalSeparation, yPosition, figWidth - horizontalSeparation, buttonHeight], 'String', ['Sample ' num2str(experimentNumber) ': Select the adjectives which best describe the audio sample.'], 'BackgroundColor', windowColour, 'FontSize', 14, 'FontWeight', 'bold');

% populate the window with buttons
global adjectiveSelected buttonMatrix;
adjectiveSelected = zeros(1, numAdjectives);
buttonMatrix = zeros(1, numAdjectives);

nonSelectedColour = [172 23 31]/255;
selectedColour = [30 144 255]/255;

for adjectiveNumber = 1:numAdjectives
	rowNumber = ceil(adjectiveNumber/numColumns) + 1;
	yPosition = figHeight - (rowNumber*(buttonHeight + verticalSeparation));
	
	columnNumber = mod(adjectiveNumber - 1, numColumns);
	xPosition = columnNumber*(buttonWidth + horizontalSeparation) + horizontalSeparation;

	buttonMatrix(adjectiveNumber) = uicontrol(fig, 'Style', 'checkbox', 'Position', [xPosition, yPosition, buttonWidth, buttonHeight], 'String', adjectives{adjectiveNumber}, 'BackgroundColor', windowColour, 'CallBack' , ['AdjectiveValidationCallbacks(''toggleAdjectiveSelection'',' num2str(adjectiveNumber) ')'], 'FontSize', 10, 'FontWeight', 'bold');
end

xPosition = figWidth/2 - horizontalSeparation - 1.5*buttonWidth;
playButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Play Sample', 'CallBack', 'AdjectiveValidationCallbacks(''playSample'')');

xPosition = xPosition + horizontalSeparation + buttonWidth;
stopButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Stop Audio', 'CallBack', 'AdjectiveValidationCallbacks(''stopAudio'')');

xPosition = xPosition + horizontalSeparation + buttonWidth;
continueButton = uicontrol(fig, 'Style', 'pushbutton', 'Position', [xPosition, verticalSeparation, buttonWidth, buttonHeight], 'String', 'Continue', 'CallBack', ['AdjectiveValidationCallbacks(''proceed'',' num2str(experimentNumber) ')']);
