function GetAdjectives(config)
% Start eh adjective validation process

if ~exist(config, 'file')
    errordlg('The configuration file you have requested does not exist.');
    clear global;
    return;
end
file = fopen(config, 'r');

[configFilePath configFileName configFileExtension] = fileparts(config);

% parse the file
sampleStrings = cell(0);
global adjectives;
adjectives = cell(0);
block = 0;
lineInBlock = 0;
while true
	currentLine = fgetl(file);
	if currentLine == -1
		break
	end

	if prod(double(isspace(currentLine)))
		if block > 2
			break;
		end
		lineInBlock = 0;
		continue;
	else
		lineInBlock = lineInBlock + 1;
	end

	if lineInBlock == 1
		block = block + 1;
	end

	switch block
		case 1
			sampleStrings = [sampleStrings currentLine];
		case 2
			adjectives = [adjectives currentLine];
	end
end

fclose(file);

numSamples = length(sampleStrings);
numAdjectives = length(adjectives);

% check that all samples are valid audio files then read into samples array
global samples;
samples = cell(numSamples, 2);
for experimentNumber = 1:numSamples
	if strcmp(wavfinfo(sampleStrings{experimentNumber}), '')
	    errordlg(['Sample Number ' num2str(experimentNumber) ' Is Not A Valid Wave File']);
	    return;
	else
	    [samples{experimentNumber, 1}, samples{experimentNumber, 2}] = wavread(sampleStrings{experimentNumber});
	end
end

% save file stuff
if ~isequal(configFilePath, '')
    configFilePath = [configFilePath '\'];
end

% ask subject for a save file location
global resultsFileLocation;

[resultsFileName, resultsFilePath] = uiputfile([configFilePath 'MyResults.adj'], 'Name Results File');
resultsFileLocation = [resultsFilePath resultsFileName];

% save to a file
if resultsFileName == 0
	clear global;
	return;
else
	results = AdjectiveValidationResult(numSamples, numAdjectives);
	results.experimentNumber = 1;
	save(resultsFileLocation, 'results');
end


AdjectiveValidation(1);
