function loopSample(audioHandle, otherGuff)
	% a little function to allow audio to loop without blocking
	if audioHandle.UserData.keepPlaying
		play(audioHandle);
	end
end

