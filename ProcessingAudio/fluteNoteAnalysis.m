players = {'A', 'B', 'C'};
notes = {'D4', 'E4', 'FSharp4', 'G4', 'A4', 'B4', 'CSharp5', 'D5', 'E5', 'FSharp5', 'G5', 'A5', 'B5'};

fluteSamples = cell(3, 13);

if ~exist('Graphs')
	mkdir('Graphs');
end
	
figureNumber = 1;
for playerIndex = 1:length(players)

	player = players{playerIndex};

	for noteIndex = 1:length(notes);

		note = notes{noteIndex};

		sampleString = ["FluteSamples/Player" player note ".wav"];
		[tempData, tempFs] = wavread(sampleString);
		tempStruct.data = tempData;
		tempStruct.fs = tempFs;
			
		tempFig = figure(figureNumber);
		set(tempFig, 'visible', 'off');
		[tempStruct.orders tempStruct.amplitudes tempStruct.f0] = relativeHarmonicAmplitudes(tempData, tempFs, true, true);
		title(["Player " player ": " note]);
		xlabel("Overtone");
		ylabel("Relative Amplitude");

		fluteSamples{playerIndex, noteIndex} = tempStruct;

		fileString = ["Graphs/Player" player note "Harmonics.eps"];
		print(fileString, '-deps', '-color');

		++figureNumber;
	end

end
