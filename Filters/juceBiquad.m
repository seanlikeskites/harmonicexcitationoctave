function out = juceBiquad(in, fs, fc)

n = 1 / tan(pi * fc / fs);
c1 = 1 / (1 + n*sqrt(2) + n^2);

num = [c1, c1*2, c1];
den = [1, c1*2*(1 - n^2), c1*(1 - n*sqrt(2) + n^2)];

out = filter(num, den, in);
