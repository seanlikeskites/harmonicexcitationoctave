function out = iirHilbert(in)
% A function which uses a series of all pass filters to create a quadrature signal for a given input signal.
%
% Usage: out = iirHilbert(in)
%
% Return Values:
% (out) is the generated quadrature signal.
%
% Arguments:
% (in) is the input signal you wish to generate a quadrature signal for.

% define the coefficients, took these from Ollie Niemitalo's website
realCoeffs = [0.6923878, 0.9360654322959, 0.9882295226860, 0.9987488452723];
imagCoeffs = [0.4021921162426, 0.8561710882420, 0.9722909545651, 0.9952884791278];

% filter the signals
realSignal = in;
imagSignal = in;
for k = 1:4
	tempRealNum = [realCoeffs(k)^2, 0, -1];
	tempRealDen = [1, 0, -realCoeffs(k)^2];
	
	tempImagNum = [imagCoeffs(k)^2, 0, -1];
	tempImagDen = [1, 0, -imagCoeffs(k)^2];

	realSignal = filter(tempRealNum, tempRealDen, realSignal);
	imagSignal = filter(tempImagNum, tempImagDen, imagSignal);
end

% delay by one sample
realSignal = filter([0, 1], 1, realSignal);

out = realSignal - i*imagSignal;
