function out = singleHarmonicBrightness(in, fs, f0, newCentroid, method)
% A function which introduces individual harmonics to change the spectral centroid of a signal.
%
% Usage: out = singleHarmonicBrightness(in, fs, f0, newCentroid, method)
% 
% Return Values:
% (out) is the processed signal.
%
% Arguments:
% (in) is the signal to be processed.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (newCentroid) is the desired spectral centroid for the processed signal.
% (method) is the method used to generate the harmonics.
%	 	Use @iap for instantaneous amplitude and phase.
%		Use @ssb for single side band automodulation.

signalLength = length(in);
fifthToTenthHarmonics = zeros(1, signalLength);
for harmonicNumber = 5:10
	fifthToTenthHarmonics += method(in, fs, f0, harmonicNumber, 500);
end

eleventhToSixteenthHarmonics = zeros(1, signalLength);
for harmonicNumber = 21:26
	eleventhToSixteenthHarmonics += method(in, fs, f0, harmonicNumber, 500);
end

out = moveSpectralCentroid(in, fifthToTenthHarmonics, fs, newCentroid);
out = moveSpectralCentroid(out, eleventhToSixteenthHarmonics, fs, newCentroid);
