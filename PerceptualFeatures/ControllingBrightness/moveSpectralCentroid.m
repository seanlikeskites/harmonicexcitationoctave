function out = moveSpectralCentroid(in1, in2, fs, newCentroid)
% A function which combines two signals to achieve a given spectral centroid.
%
% Usage: out = moveSpectralCentroid(in1, in2, fs, newCentroid)
% 
% Return Values:
% (out) is the processed signal.
%
% Arguments:
% (in1) is the first input signal.
% (in2) is the second input signal.
% (fs) is the sample rate of those signals.
% (f0) is the fundamental frequency of those signals.
% (newCentroid) is the desired spectral centroid for the processed signal.

% find the centroids of the input signals
in1Centroid = spectralCentroid(in1, fs);
in2Centroid = spectralCentroid(in2, fs);

% deal with cases where the new centroid value is not attainable
if in1Centroid <= in2Centroid

	if newCentroid <= in1Centroid
		out = in1;
		return;
		
	elseif newCentroid >= in2Centroid
		out = in2;
		return;

	end

elseif in1Centroid > in2Centroid

	if newCentroid <= in2Centroid
		out = in2;
		return;

	elseif newCentroid >= in1Centroid
		out = in1;
		return;

	end

end

% move the centroid if possible
scaleFactor = (newCentroid - in1Centroid)/(in2Centroid - in1Centroid);
out = (1 - scaleFactor)*in1 + scaleFactor*in2;
