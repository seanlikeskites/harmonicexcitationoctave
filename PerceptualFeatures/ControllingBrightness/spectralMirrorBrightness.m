function out = spectralMirrorBrightness(in, fs, newCentroid)
% A function which uses a spectral mirroring effect to change the spectral centroid of a signal.
%
% Usage: out = spectralMirrorBrightness(in, fs, newCentroid)
% 
% Return Values:
% (out) is the processed signal.
%
% Arguments:
% (in) is the signal to be processed.
% (fs) is the sample rate of that signal.
% (newCentroid) is the desired spectral centroid for the processed signal.

% flip the spectrum of the input signal 
mirroredSignal = in;
mirroredSignal(2:2:end) *= -1;

% move the spectral centroid
out = moveSpectralCentroid(in, mirroredSignal, fs, newCentroid);
