function out = spectralShiftBrightness(in, fs, f0, newCentroid)
% A function which uses a spectral shifting effect to change the spectral centroid of a signal.
%
% Usage: out = spectralShiftBrightness(in, fs, newCentroid)
% 
% Return Values:
% (out) is the processed signal.
%
% Arguments:
% (in) is the signal to be processed.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (newCentroid) is the desired spectral centroid for the processed signal.  

% get the centroid of the input signal
inputCentroid = spectralCentroid(in, fs);

% work out an base amount of shift to apply and apply it
shiftFactor = round((newCentroid - inputCentroid)/(0.5*f0));
shiftedSignal = spectralShift(in, fs, f0, shiftFactor);

% shift the centroid of the signal
scaleFactor = (newCentroid - inputCentroid)/(shiftFactor*f0);
out = (1 - scaleFactor)*in + scaleFactor*shiftedSignal;
