function w = warmth(in, fs, f0, skirt, includeInharmonicPartials)
% A function which calculates the perceived warmth of a signal.
%
% Usage: w = warmth(in, fs, f0, skirt)
% 
% Return Values:
% (w) is the perceived warmth of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (skirt) is for use when f0 will not lie in a DFT frequency bin. The algorithm will look this many frequency bins to each side to get a more accurate amplitude.
% (includeInharmonicPartials) is a boolean value to set whether the calculation is done using only harmonics or the entire spectrum.

if includeInharmonicPartials
	% find the magnitude spectrum of the signal
	complexDFT = fft(in);
	magnitudeSpectrum = abs(complexDFT);

	% get only the positive frequencies
	signalLength = length(magnitudeSpectrum);
	nyquistBin = floor(signalLength / 2) + 1;
	positiveMagnitudeSpectrum = magnitudeSpectrum(1:nyquistBin);
	positiveMagnitudeSpectrum(2:end-1) *= 2;

	% calculate the bin size
	binSize = fs/signalLength;

	% find corresponding bin
	cutoffFrequency = 3.5*f0;
	cutoffBin = round(cutoffFrequency/binSize) + 1;

	% get energies in the first three and the rest of the harmonics
	firstThreeHarmonics = sum(positiveMagnitudeSpectrum(1:cutoffBin).^2);
	remainingHarmonics = sum(positiveMagnitudeSpectrum(cutoffBin+1:end).^2);

	% calculate ratio 
	w = firstThreeHarmonics/remainingHarmonics;
else
	% get amplitudes of harmonics
	amplitudes = harmonicLevels(in, fs, f0, skirt);

	if length(amplitudes) < 4
		disp('I cannot compute the warmth for this signal. The sample rate is not high enough to allow for enough harmonic frequencies.');
		return;
	end

	% get energies in the first three and the rest of the harmonics
	firstThreeHarmonics = sum(amplitudes(1:3).^2);
	remainingHarmonics = sum(amplitudes(4:end).^2);

	% calculate ratio 
	w = firstThreeHarmonics/remainingHarmonics;
end
