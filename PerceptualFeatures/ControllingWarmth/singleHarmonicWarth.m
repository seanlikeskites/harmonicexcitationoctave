function out = singleHarmonicWarmth(in, fs, f0, newWarmth, method)
% A function which introduces individual harmonics to change the warmth of a signal.
%
% Usage: out = singleHarmonicWarmth(in, fs, f0, newWarmth, method)
% 
% Return Values:
% (out) is the processed signal.
%
% Arguments:
% (in) is the signal to be processed.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (newWarmth) is the desired warmth for the processed signal.
% (method) is the method used to generate the harmonics.
%	 	Use @iap for instantaneous amplitude and phase.
%		Use @ssb for single side band automodulation.

out = in;
