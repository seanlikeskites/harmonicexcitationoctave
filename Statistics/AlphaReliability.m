function A = AlphaReliability(data)
% A function which calculates Cronbach's Alpha Reliability Coefficient.
%
% Usage: A = AlphaReliability(data)
%
% Return Values:
% (A) is the Alpha Reliability Coefficient.
%
% Arguments:
% (data) is the input data. It should take the form of a 2 dimensional matrix. Each column contains an individual subjects responses.

k = size(data, 1);

sigmaX = var(sum(data));
sigmaY = sum(var(data'));

A = k/(k - 1) * (1 - sigmaY/sigmaX);
