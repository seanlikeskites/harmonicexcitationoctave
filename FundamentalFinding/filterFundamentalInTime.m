function f0 = filterFundamentalInTime(in, fs, frameSize, trackingMethod, firLen)
% A function which will separate a signal into frames and isolate the fundamental in each frame. 
%
% Usage: f0 = filterFundamentalInTime(sig, fs, frameSize, firLen, trackingMethod)
%
% Return Values:
% (f0) is a signal comprised of the isolated fundamentals from each frame of the input signal.
%
% (in) is the input signal to be processed.
% (fs) is the sample rate of that signal.
% (frameSize) is the length of the analysis frames in samples.
% (trackingMethod) is the method used to approximate the fundamental in each frame.
%		Use ('zc') for a zero crossing method.
%		Use ('ac') for an autocorrelation method.
% (firLen) is the length of the fir kernal used to filter the signal.

% separate the input into frames and find the fundamental frequency in each
[fundamentals, frameArray] = trackFundamental(in, fs, frameSize, trackingMethod);
numFrames = size(frameArray, 2);

% add some zeros to catch the decay of the fundamental isolation filter
frameArray = [frameArray; zeros(firLen - 1, numFrames)];

% loop through the frames
for frameNumber = 1:numFrames
	% filter each frame to isolate its fundamental
	if nargin < 5
		frameArray(:, frameNumber) = isolateFundamental(frameArray(:, frameNumber), fs, fundamentals(frameNumber));
	else
		frameArray(:, frameNumber) = isolateFundamental(frameArray(:, frameNumber), fs, fundamentals(frameNumber), firLen);
	end

	% add the decay of the previous frames filter to the start of the next frame
	if frameNumber > 1
		frameArray(1:firLen - 1, frameNumber) += frameArray(end - (firLen - 2):end, frameNumber - 1);
	end
end

% remove the filter decays from the bottom of the array
frameArray = frameArray(1:frameSize, :);

% put the frames back into one long array
f0 = buffer(frameArray, numel(frameArray));
