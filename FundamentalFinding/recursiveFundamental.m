function f0 = recursiveFundamental(in, fs)
% A function which uses the the recursive method from Larsen and Aarts (2004) approximate the fundamental frequency of a signal.
%
% Usage: f0 = recursiveFundamental(in, fs)
%
% Return Values:
% (f0) is the approximate fundamental frequency of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% pad the signal
if size(in, 2) == 1
	in = in';
end
paddedSignal = [0, 0, in];

signalLength = length(in);
fundamentalSignal = zeros(1, signalLength + 1);

gamma = 0.002;

for i = 3:(signalLength + 2)
	fundamentalSignal(i) = fundamentalSignal(i - 1) + paddedSignal(i - 1)*gamma*(paddedSignal(i) + paddedSignal(i - 2) - 2*paddedSignal(i - 1)*fundamentalSignal(i - 1));
end

f0 = fs*acos(fundamentalSignal)/(2*pi);
