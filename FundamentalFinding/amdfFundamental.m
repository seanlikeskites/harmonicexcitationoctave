function f0 = amdfFundamental(in, fs)
% A function which uses the average magnitude difference function to approximate the fundamental frequency of a signal.
%
% Usage: f0 = amdfFundamental(in, fs)
%
% Return Values:
% (f0) is the approximate fundamental frequency of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% calculate the maximum lag
lowestFrequency = 20;
longestPeriod = 1/lowestFrequency;
maxLag = fs*longestPeriod;

signalLength = length(in);
if maxLag > signalLength
	maxLag = signalLength;
end

% pad the signal
if size(in, 2) == 1
	in = in';
end
paddedSignal = [in zeros(1, maxLag)];

% calculate the mean difference for each lag value
amdfValues = zeros(1, maxLag - 1);
for lag = 2:maxLag;
	difference = abs(in - paddedSignal((1:signalLength) + lag));
	amdfValues(lag - 1) = mean(difference);
end

% find the first negative gradient in the amdfValues
firstDifference = diff(amdfValues);
firstNegativeGradient = find(firstDifference < 0, 1);

% deal with the problem that there might not be a negative gradient
if isempty(firstNegativeGradient)
	f0 = 0;
	return;
end

% calculate the fundamental
[certainty, fundamentalPeriod] = min(amdfValues(firstNegativeGradient:end));
f0 = fs/(fundamentalPeriod + firstNegativeGradient);
