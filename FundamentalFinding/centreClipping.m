function out = centreClipping(in, threshold)
% A function which centre clips a signal
%
% Usage: out = centreClipping(in, threshold)
%
% Return Values:
% (out) is the clipped signal.
%
% Arguments:
% (in) is the signal to be clipped
% (threshold) is the clipping threshold.

% clip samples below the treshold to zero
peaksOnly = in.*(abs(in) > threshold);

% offset the remaining peaks
offset = sign(peaksOnly)*threshold;
out = peaksOnly - offset;
