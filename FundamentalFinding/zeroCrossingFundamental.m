function f0 = zeroCrossingFundamental(in, fs)
% A function which counts the zero crossings in a signal to approximate its fundamental frequency.
%
% Usage: f0 = zeroCrossing(in, fs)
%
% Return Values:
% (f0) is the approximate fundamental frequency of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% make a filter to reduce influence of higher harmonics
[b, a] = butter(2, 0.01);

% filter the input signal
filtered = filter(b, a, in);

% count the zero crossings in the signal
crossings = find(diff(filtered > 0) ~= 0) + 1;
numCrossings = length(crossings);
numPeriods = numCrossings/2;

% calculate the fundamental frequency in hertz
duration = length(in)/fs;
f0 = numPeriods/duration;
