function f0 = dftFundamental(in, fs)
% A function which uses the DFT to approximate the fundamental frequency of a signal.
%
% Usage: f0 = dftFundamental(in, fs)
%
% Return Values:
% (f0) is the approximate fundamental frequency of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% transpose input if needed
if size(in, 1) > 1
	in = in';
end

% get the spectral magnitudes
signalLength = length(in);
spectralMagnitudes = abs(fft(in));
spectrumLength = floor(signalLength / 2) + 1;
spectrum = spectralMagnitudes(1:spectrumLength);
spectrum(2:end-1) *= 2;

% hps it up
maxDownsampleFactor = floor(spectrumLength / 4);
if maxDownsampleFactor > 5
	maxDownsampleFactor = 5;
end

numBins = ceil(spectrumLength / maxDownsampleFactor);
amplitudeProduct = ones(1, numBins);

for downsampleFactor = 1:maxDownsampleFactor;
	amplitudeProduct .*= spectrum(1:downsampleFactor:numBins*downsampleFactor);
end

[maxAmplitudeProduct maxAmplitudeIndex] = max(amplitudeProduct);

frequencies = fs * (0:signalLength - 1) / signalLength; 
f0 = frequencies(maxAmplitudeIndex);
