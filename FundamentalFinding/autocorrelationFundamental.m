function f0 = autocorrelationFundamental(in, fs)
% A function which uses autocorrelation to approximate the fundamental frequency of a signal.
%
% Usage: f0 = autocorrelation(in, fs)
%
% Return Values:
% (f0) is the approximate fundamental frequency of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% take the second half of the autocorrelation (the first half is just a mirror image)
len = length(in);
correlation = xcorr(in ,in);
secondHalf = correlation(len + 1:end);
%plot(secondHalf);

% ignore the first peak by finding the first part of the autocorrelation with a positive gradient
firstDifference = diff(secondHalf);
firstPositiveGradient = find(firstDifference > 0, 1);

% deal with the problem that there might not be a positive gradient
if isempty(firstPositiveGradient)
	f0 = 0;
	return;
end

% find the tallest peak in the remaining signal
[maxValue, maxValueIndex] = max(secondHalf(firstPositiveGradient:end));

% use the location of this peak to calculate the fundamental frequency
fundamentalPeriod = maxValueIndex + firstPositiveGradient - 1;
f0 = fs/fundamentalPeriod;
