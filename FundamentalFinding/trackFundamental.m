function [f0, frameArray] = trackFundamental(in, fs, frameSize, method)
% A function to track the fundamental frequency in a signal. The signal is split up into analysis frames (each of length frameSize). The fundamental frequency of each of these frames is approximated and concatenated into an array. 
%
% Usage: [f0, frameArray] = trackFundamental(in, fs, frameSize, method)
%
% Return Values:
% (f0) is an array which describes the changes in fundamental frequency of the input signal. Each separate value represents the fundamental frequency of a single analysis frame.
% (frameArray) is an array which holds the original analysis frames.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (frameSize) is the length of the analysis frames in samples.
% (method) is a handle to the function to use for f0 detection.
%		Use @zeroCrossingFundamental for a zero crossing method.
%		Use @autocorrelationFundamental for an autocorrelation method.
%		Use @amdfFundamental for the average magnitude difference function.
%		Use @dftFundamental for a DFT based method.
%		Use @flwtFundamental for the fast lifting wavelet transform method.

% split the input into frames
frameArray = buffer(in, frameSize);
numFrames = size(frameArray, 2);

% approximate the fundamental frequency in each frame
f0 = zeros(1, numFrames);
for frameNumber = 1:numFrames

   	frame = frameArray(:, frameNumber);

	f0(frameNumber) = method(frame, fs);

end
