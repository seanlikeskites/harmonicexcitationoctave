% Run this scrip to add all the subdirectories to your path.

addpath([pwd '/GenerationAlgorithms']);
addpath([pwd '/GenerationAlgorithms/HarmonicsVsAmplitude']);
addpath([pwd '/GenerationAlgorithms/Graphs']);
addpath([pwd '/FundamentalFinding']);
addpath([pwd '/PerceptualFeatures']);
addpath([pwd '/PerceptualFeatures/ControllingBrightness']);
addpath([pwd '/PerceptualFeatures/Loudness']);
addpath([pwd '/Tools']);
addpath([pwd '/LowLevelFeatures']);
addpath([pwd '/Statistics']);
addpath([pwd '/Filters']);
addpath([pwd '/SMCPlots']);
addpath([pwd '/Plots']);
addpath([pwd '/Systems']);
