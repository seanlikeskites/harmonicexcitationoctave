function out = getHilbertResponse(frequencies, order)

out = zeros(1, length(frequencies));

for i = 1:length(frequencies)
	for n = 1:2:order
		out(i) += 2/(n*pi) * (exp(j*frequencies(i))^(-n) - exp(j*frequencies(i))^n);
	end
end
