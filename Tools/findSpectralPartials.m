function [Fp Ap] = findSpectralPartials(in, fs, peakThresh=500, graph = false)
% A function which finds the amplitudes and frequencies of the spectral partials of a signal.
%
% Usage: [Fp Ap] = findSpectralPartials(in, fs, graph)
% 
% Return Values:
% (Fp) is a vector containing the frequencies of the separate partials in the input signal.
% (Ap) is a vector containing the amplitudes of the separate partials in the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (graph) is an optional boolean argument which controls whether a graph of the partials is plotted.

% find the magnitude spectrum of the signal
complexDFT = fft(in);
magnitudeSpectrum = abs(complexDFT);

% get only the positive frequencies
signalLength = length(magnitudeSpectrum);
nyquistBin = floor(signalLength / 2) + 1;
positiveMagnitudeSpectrum = magnitudeSpectrum(1:nyquistBin);
positiveMagnitudeSpectrum(2:end-1) *= 2;

% create some initial conditions for peak detection
maxAmplitude = max(positiveMagnitudeSpectrum);
minPeakHeight = maxAmplitude/peakThresh;

binSize = fs/signalLength;
initialPeakDistance = 20/binSize;

% find the peaks
[peaks, indxs] = findPeaks(positiveMagnitudeSpectrum, minPeakHeight, initialPeakDistance, 8);

%% first peak detection
%[peakAmplitudes peakLocations] = peakDetector(positiveMagnitudeSpectrum, 'MinPeakHeight', minPeakHeight, 'MinPeakDistance', initialPeakDistance);
%
%% constrain the peak detection results
%if length(peakLocations) > 1
%	meanPeakSeparation = mean(diff(peakLocations));
%else
%	meanPeakSeparation = 1;
%end
%
%%disp(meanPeakSeparation);
%
%% second peak detection
%[peakAmplitudes peakLocations] = peakDetector(positiveMagnitudeSpectrum, 'MinPeakHeight', minPeakHeight, 'MinPeakDistance', meanPeakSeparation/2);

Ap = peaks(:)'/signalLength;
Fp = (indxs(:)' - 1)*binSize;

% plot the results if necessary
if graph
	hideFigures(100);
	plot(positiveMagnitudeSpectrum);

	hold on;
	scatter(indxs, peaks);
	hold off;
end
