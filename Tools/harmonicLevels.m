function Ah = harmonicLevels(in, fs, f0, skirt)
% A function which calculates the amplitudes of separate harmonics in a signal.
%
% Usage: Ah = harmonicLevels(in, fs, f0, skirt)
% 
% Return Values:
% (Ah) is a vector containing the amplitudes of the separate harmonics in the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (skirt) is for use when f0 will not lie in a DFT frequency bin. The algorithm will look this many frequency bins to each side to get a more accurate amplitude.

% find the magnitude spectrum of the signal
complexDFT = fft(in);
magnitudeSpectrum = abs(complexDFT);

% get only the positive frequencies
signalLength = length(magnitudeSpectrum);
magnitudeSpectrum /= signalLength;
nyquistBin = floor(signalLength / 2) + 1;
positiveMagnitudeSpectrum = magnitudeSpectrum(1:nyquistBin);
positiveMagnitudeSpectrum(2:end-1) *= 2;

% calculate the bin size
binSize = fs/signalLength;

% how many harmonics are there before the nyquist
maxHarmonic = fix(fs/(2*f0));

% look for the level of each harmonic
amplitudes = zeros(1, maxHarmonic);
for harmonic = 1:maxHarmonic
	harmonicFrequency = f0*harmonic;
	binNumber = round(harmonicFrequency/binSize) + 1;

	lowerBin = binNumber - skirt;
	if lowerBin < 1
		lowerBin = 1;
	end

	upperBin = binNumber + skirt;
	if upperBin > nyquistBin
		upperBin = nyquistBin;
	end

	amplitudes(harmonic) = sqrt(sum(positiveMagnitudeSpectrum(lowerBin:upperBin).^2));
end

Ah = amplitudes;
