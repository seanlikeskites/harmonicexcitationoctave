freqs = -pi:0.001:pi;

lowOrder = getHilbertResponse(freqs, 11);
highOrder = getHilbertResponse(freqs, 101);

% plot magnitudes
hideFigures(1);
plot(freqs, [20*log10(abs(lowOrder)); 20*log10(abs(highOrder))], 'linewidth', 2);
set(gca, 'fontsize', 20);
xlabel('Frequency (Hz)');
ylabel('Amplitude (dB)');
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
legend('M = 11', 'M = 101');
axis([-pi, pi, -60, 10]);
set(gca, 'xtick', [-pi, 0, pi], 'xticklabel', {'-f_{s}/2', '0', 'f_{s}/2'});
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Tools/HilbertMagnitudeResponses.eps', '-FArial:18', '-deps', '-color');


% plot phases
hideFigures(2);
plot(freqs, [arg(lowOrder); arg(highOrder)], 'linewidth', 2);
set(gca, 'fontsize', 20, 'interpreter', 'tex');
xlabel('Frequency (Hz)');
ylabel('Phase (radians)');
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
legend('M = 11', 'M = 101');
axis([-pi, pi, -2, 2]);
set(gca, 'xtick', [-pi, 0, pi], 'xticklabel', {'-f_{s}/2', '0', 'f_{s}/2'});
set(gca, 'ytick', [-pi/2, 0, pi/2], 'yticklabel', {'-\pi/2', '0', '\pi/2'});
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Tools/HilbertPhaseResponses.eps', '-FArial:18', '-deps', '-color');

% iir
filter1 = getAllpassResponse(freqs, 0.6923878).*getAllpassResponse(freqs, 0.9360654322959).*getAllpassResponse(freqs,
0.988229522686).*getAllpassResponse(freqs, 0.9987488452737)./exp(j*freqs);
filter2 = getAllpassResponse(freqs, 0.4021921162426).*getAllpassResponse(freqs, 0.8561710882420).*getAllpassResponse(freqs,
0.9722909545651).*getAllpassResponse(freqs, 0.9952884791278);

hideFigures(3);
plot(freqs, -sign(freqs).*mod(arg(filter2) - arg(filter1), pi), 'linewidth', 2);
set(gca, 'fontsize', 20, 'interpreter', 'tex');
xlabel('Frequency (Hz)');
ylabel('Phase (radians)');
set(get(gca, 'xlabel'), 'fontsize', 20);
set(get(gca, 'ylabel'), 'fontsize', 20);
axis([-pi, pi, -3, 3]);
set(gca, 'xtick', [-pi, 0, pi], 'xticklabel', {'-f_{s}/2', '0', 'f_{s}/2'});
set(gca, 'ytick', [-pi/2, 0, pi/2], 'yticklabel', {'-\pi/2', '0', '\pi/2'});
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Tools/IIRHilbertPhaseResponses.eps', '-FArial:18', '-deps', '-color');


iirResponse = -sign(freqs).*mod(arg(filter2) - arg(filter1), pi);
save('Tools/HilbertData.mat', 'freqs', 'lowOrder', 'highOrder', 'iirResponse');
