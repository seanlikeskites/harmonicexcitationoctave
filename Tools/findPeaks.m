function [amps, idxs] = findPeaks(in, thresh, sep, wid)
% quick and memory friendly peak finding

if wid > sep
	error('wid should not be greater than sep');
end

sep = round(sep);
widPad = round(wid/2);

% threshold things
in = in.*(in > thresh);

% pad the signal
padded = [zeros(1, sep), in, zeros(1, sep)];

% find the peaks
peaks = zeros(1, length(in));
i = 1;

while i < length(in)
	sigI = i + sep;
	sample = in(i);

	if ~sum(sample <= [padded(i:sep), padded((sigI+1):sigI+sep)])
		peaks(i) = 1;
		i += sep;
	elseif (range(padded((sigI-widPad):(sigI+widPad))) == 0) && ~sum(sample <= [padded(i:(sep - widPad)), padded((sigI+1+widPad):sigI+sep)])
		peaks(i) = 1;
		i += sep;
	else
		++i;
	end
end

idxs = find(peaks);
amps = in(idxs);
