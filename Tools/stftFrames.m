function [frames, numFrames] = stftFrames(in, windowSize, hopSize)
% A function which separates the input signal in frames.
%
% Usage: [frames, numFrames] = stftFrames(in, windowSize, hopSize)
%
% Return Values:
% (frames) is a matrix containing the framed audio data.
%		Each row of the matrix is a single frame.
% (numFrames) is the number of frames in the frames matrix.
%
% Arguments:
% (in) is the signal to be split into frames.
% (windowSize) is the length of each frame in samples.
% (hopSize) is the number of samples from the original signal that separate the start of each frame.

% transpose input if necessary
if size(in, 1) > size(in, 2)
	in = in';
end

signalLength = length(in);

% calculate the number of frames
numFrames = ceil((signalLength - windowSize) / hopSize) + 1;

% add some zeros to the end of the input so all frames are filled
totalFramesLength = numFrames * windowSize;
if totalFramesLength > signalLength
	in = [in zeros(1, totalFramesLength - signalLength)];
end

% fill the frame matrix
frames = zeros(numFrames, windowSize);
for frameNumber = 1:numFrames
	frameStart = (frameNumber - 1) * hopSize + 1;
	frameEnd = frameStart + windowSize - 1;

	frameData = in(frameStart:frameEnd);

	frames(frameNumber, :) = frameData;
end
