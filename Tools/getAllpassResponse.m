function out = getAllpassResponse(frequencies, coeff)

out = zeros(1, length(frequencies));

for i = 1:length(frequencies)
	out(i) += (coeff^2 - exp(j*frequencies(i))^(-2)) / (1 - coeff^2*exp(j*frequencies(i))^(-2));
end
