function out = sampleCenteredDiff(in)
%A function which finds the first difference of a signal. For the samples in the middle of the input the value calculated is the mean of the diferences either side of it. For samples at the end it is assumed that the gradient remains constant into the unknown samples.
%
% Usage: out = sampleCenteredDiff(in)
% 
% Return Values:
% (out) is the resultant first difference signal.
%
% Arguments:
% (in) is the signal to be processed.

% find the length of the input signal
signalLength = length(in);

% take the differences either side of the samples
rightDiff = diff(in(2:end));
leftDiff = diff(in(1:end-1));

% find the mean differences
middleDiff =(rightDiff + leftDiff)/2;

% put it all into one signal
out = zeros(1, signalLength);
out(1) = leftDiff(1);
out(end) = rightDiff(end);
out(2:end-1) = middleDiff;
