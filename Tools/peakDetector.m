function [Ap Ip] = peakDetector(in, varargin)
% A function which finds the peaks in a signal.
%
% Usage: [Ap Ip] = peakDetector(in, varargin)
% 
% Return Values:
% (Ap) is a vector containing the amplitudes of the found peaks.
% (Ip) is a vector containing the locations of the found peaks.
%
% Arguments:
% (in) is the signal to be analysed.
% Other arguments are passed as pairs of parameter name and parameter value:
%	e.g. [amplitudes locations] = peakDetector(signal, 'MinPeakHeight', 0.4)	
%	There are 2 parameters at current:
%		'MinPeakHeight' sets the minimum peak height to detect.
%		'MinPeakDistance' sets the minimum distance between peaks for them to be considered separate.

% deal with the parameters
numOptions = length(varargin);

minPeakHeight = min(in);
minPeakDistance = 1;

for option = 1:2:numOptions

	if strcmp(varargin{option}, "MinPeakHeight") && isnumeric(varargin{option + 1})

		minPeakHeight = varargin{option + 1};

	elseif strcmp(varargin{option}, "MinPeakDistance") && isnumeric(varargin{option + 1})

		minPeakDistance = varargin{option + 1};

	else
		error(["Problem with input argument " num2str(option)]);
	end

end

if size(in, 1) > 1
	in = in';
end

% find the peaks and troughs in the signal
firstDifference = [1, diff(in)];
minimaAndMaxima = [0, diff(firstDifference > 0) ~= 0];

% find only the peaks
secondDifference = [1, 1, diff(in, 2)];
broadMaxima = secondDifference < 0;

maxima = (minimaAndMaxima & broadMaxima);
maximaIndicies = find(maxima) - 1;

% ignore peaks below the minimum peak height
maximaIndiciesAboveThreshold = maximaIndicies(in(maximaIndicies) > minPeakHeight);

% find out how close each peak is to all the others
differenceMatrix = abs(bsxfun(@minus, maximaIndiciesAboveThreshold', maximaIndiciesAboveThreshold));

% separate peaks which are the right distance apart
if any((differenceMatrix(:) < minPeakDistance) & differenceMatrix(:) > 0)

	numPeaks = length(maximaIndiciesAboveThreshold);

	peakIncluded = zeros(1, numPeaks);

	% for each peak we find the peaks closer than the minimum distance
	% if the peak in question has a higher amplitude than the others it is included in the output.
	% if there are several maximum peaks, the one which comes first is chosen
	for peak = 1:numPeaks
		distances = differenceMatrix(peak, :);
		closePeaks = distances < minPeakDistance;
		closePeakIndicies = maximaIndiciesAboveThreshold(closePeaks);

		peakIndex = maximaIndiciesAboveThreshold(peak);
		peakAmplitude = in(peakIndex);
		closePeakAmplitudes = in(closePeakIndicies);

		[maxPeakAmplitude, maxPeakIndex] = max(closePeakAmplitudes);
		
		if (peakAmplitude == maxPeakAmplitude) && (peakIndex == closePeakIndicies(maxPeakIndex))
			peakIncluded(peak) = 1;
		end

	end

	peakIncludedIndicies = find(peakIncluded);
	isolatedPeakIndicies = maximaIndiciesAboveThreshold(peakIncludedIndicies);

else
	isolatedPeakIndicies = maximaIndiciesAboveThreshold;
end

% output
Ip = isolatedPeakIndicies;
Ap = in(Ip);
