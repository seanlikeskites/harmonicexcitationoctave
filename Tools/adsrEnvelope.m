function E = adsrEnvelope(attack, decay, sustain, release, length, fs)
% A function to generate an ADSR envelope.
%
% E = adsrEnvelope(attack, decay, sustain, release, length, fs)
% 
% Return Values:
% (E) is the ADSR envelope signal.
%
% Arguments:
% (attack) is the attack time in miliseconds.
% (decay) is the decay time in miliseconds.
% (sustain) is the sustain level.
% (release) is the release time in miliseconds.
% (length) is the length of the envelope in samples.
% (fs) is the sample rate of the signal.

% find different lengths in samples
attackSamples = attack * fs / 1000;
decaySamples = decay * fs / 1000;
releaseSamples = release * fs / 1000;
sustainSamples = length - (attackSamples + decaySamples + releaseSamples);

% make separate parts
attackEnv = linspace(0, 1, attackSamples);
decayEnv = linspace(1, sustain, decaySamples);
sustainEnv = sustain*ones(1, sustainSamples);
releaseEnv = linspace(sustain, 0, releaseSamples);

E = [attackEnv, decayEnv, sustainEnv, releaseEnv];
