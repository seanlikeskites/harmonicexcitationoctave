function N = closestNote(f)
% A function to give the note name for a given frequency.
%
% Usage: N = closestNote(f)
%
% Return Values:
% (N) is the returned note name relative to A4 of 440Hz.
%
% Arguments:
% (f) is the frequency in Hz.

semitonesFromA4 = round(12*log(f/440)/log(2));

noteChromas = {'A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'};

note = noteChromas{mod(semitonesFromA4, 12) + 1};
octave = floor((semitonesFromA4 - 3)/12) + 5;

N = [note num2str(octave)];
