function [Fh Ah] = findHarmonicPartials(in, fs, f0, tolerance)
% A function which finds the amplitudes and frequencies of the harmonic partials of a signal.
%
% Usage: [Fh Ah] = findHarmonicPartials(in, fs, f0, tolerance)
% 
% Return Values:
% (Fh) is a vector containing the frequencies of the separate partials in the input signal.
% (Ah) is a vector containing the amplitudes of the separate partials in the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of the signal.
% (tolerance) is the tolerance used to decide whether a partial is harmonic. It is given as a propotion of the fundamental.

% find the spectral partials
[f, a] = findSpectralPartials(in, fs);

% find the deviations
devs = min(mod(f, f0), mod(-f, f0));
allowableDev = f0 * tolerance;

% grab the harmonic partials
Fh = f(devs < allowableDev);
Ah = a(devs < allowableDev);

Fh = Fh(Fh > (f0 - allowableDev));
Ah = Ah(Fh > (f0 - allowableDev));
