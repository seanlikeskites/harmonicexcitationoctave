function out = changeSampleRate(in, ratio)
% A function to resample a signal.
%
% Usage: out = changeSampleRate(in, ratio)
% 
% Return Values:
% (out) is the resampled signal.
%
% Arguments:
% (in) is the signal to be resampled.
% (ratio) is the ratio of output sample rate to input sample rate.

% convert ratio to a rational fraction
[upsampleRate, downsampleRate] = rat(ratio);

signalLength = length(in);

% upsample the data
upsampledLength = signalLength * upsampleRate;
upsampled = zeros(1, upsampledLength);
for sampleIndex = 0:signalLength - 1
	upsampled(sampleIndex * upsampleRate + 1) = in(sampleIndex + 1);
end

% interpolation / anti aliasing filter
t = -51:51;
filterCutoff = 1 / upsampleRate;
filterKernel = filterCutoff*sinc(t * filterCutoff);
upsampled = filter(filterKernel, 1, upsampled);

% downsample the data
downsampledLength = upsampledLength / downsampleRate;
downsampled = zeros(1, downsampledLength);
for sampleIndex = 0:downsampledLength - 1
	downsampled(sampleIndex + 1) = upsampled(sampleIndex * downsampleRate + 1);
end

out = downsampled;
