fs = 44100;
ts = 1/fs;
t = ts:ts:7;
sig = sin(2*pi*t);

up = (1:(2*fs)) / (2*fs);
sig(1:fs) = 0;
sig((fs+1):(3*fs)) .*= up;
sig((4*fs+1):(6*fs)) .*= fliplr(up);
sig((6*fs+1):end) = 0;

envelope = abs(hilbert(sig));

hideFigures(1);
plot(t, [sig; envelope], 'linewidth', 3);
set(gca, 'fontsize', 20);
axis([0, 7, -1.3, 1.1]);
axis off;
legend('Signal', 'Amplitude Envelope', 'location', 'south');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Plots/AmplitudeEnvelope.eps',
'-FArial:18', '-deps', '-color');

attack = (0:200)/200;
decay = (200:-1:140)/200;
sustain = 140*ones(1, 600)/200;
release = (140:-0.5:0)/200;
adsr = [attack decay sustain release];

hideFigures(2);
plot(adsr, 'linewidth', 3);
set(gca, 'fontsize', 20, 'yticklabel', [], 'xticklabel', [], 'ytick', [], 'xtick', []);
line(200, [1, 0], 'linestyle', ':', 'linewidth', 2);
line(260, [140/200, 0], 'linestyle', ':', 'linewidth', 2);
line(860, [140/200, 0], 'linestyle', ':', 'linewidth', 2);
text(130, 0.3, 'Attack', 'rotation', 90, 'fontsize', 20, 'horizontalalignment', 'center', 'verticalalignment', 'middle');
text(230, 0.3, 'Decay', 'rotation', 90, 'fontsize', 20, 'horizontalalignment', 'center', 'verticalalignment', 'middle');
text(560, 0.3, 'Sustain', 'fontsize', 20, 'horizontalalignment', 'center', 'verticalalignment', 'middle');
text(940, 0.3, 'Release', 'fontsize', 20, 'horizontalalignment', 'center', 'verticalalignment', 'middle');
xlabel('Time');
ylabel('Amplitude');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Plots/ADSR.eps',
'-FArial:18', '-deps', '-color');
