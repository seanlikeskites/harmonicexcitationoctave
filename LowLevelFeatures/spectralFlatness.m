function flat = spectralFlatness(in, fs)
% A function which calculates the spectral flatness of a signal.
%
% Usage: flat = spectralFlatness(in, fs)
% 
% Return Values:
% (flat) is the spectral flatness of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% find the partials
[f, a] = findSpectralPartials(in, fs);

% calculate the flatness
flat = geomean(a) / mean(a);
