function s = spectralSlope(in, fs, f0)
% A function which calculates the spectral slope of a signal.
%
% Usage: s = spectralSlope(in, fs)
% 
% Return Values:
% (s) is the spectral slope of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.

% find the partials
[f, a] = findSpectralPartials(in, fs);

a = a(f > f0-10);
f = f(f > f0-10);

% calculate the slope
A = sum(20*log10(a));
B = sum(20*f.*log10(a));
F = sum(f);
G = sum(f.^2);
N = length(a);

s = (N*B - F*A) / (N*G - F^2);
