function sc = spectralCentroid(in, fs)
% A function which calculates the spectral centroid of a signal.
%
% Usage: sc = spectralCentroid(in, fs)
% 
% Return Values:
% (sc) is the spectral centroid of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% find the magnitude spectrum of the signal
complexDFT = fft(in);
magnitudeSpectrum = abs(complexDFT);

% get only the positive frequencies
signalLength = length(magnitudeSpectrum);
nyquistBin = floor(signalLength / 2) + 1;
positiveMagnitudeSpectrum = magnitudeSpectrum(1:nyquistBin);
positiveMagnitudeSpectrum(2:end-1) *= 2;

% work out bin frequencies
binSize = 1/signalLength;
frequencies = 0:binSize:0.5;

% calculate the average frequency
weightedAmplitudes = positiveMagnitudeSpectrum.*frequencies;
meanFrequency = sum(weightedAmplitudes)/sum(positiveMagnitudeSpectrum);

% return as a value in hertz
sc = meanFrequency*fs;
