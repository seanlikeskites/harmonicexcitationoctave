function oer = oddEvenRatio(in, fs, f0)
% A function which calculates the ratio of odd to even order harmonics in a signal.
%
% Usage: oer = oddEvenRatio(in, fs, f0)
%
% Return Values:
% (oer) is the odd even ratio of the signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (f0) is an optional argument which can be used if you already know the fundamental of the signal.

% get frequencies of fundamental and other partials
if nargin < 3
	f0 = dftFundamental(in, fs);
	if f0 == 0
		f0 = zeroCrossingFundamental(in, fs);
	end
end
a = harmonicLevels(in, fs, f0, 2);

oddA = a(1:2:end);
evenA = a(2:2:end);

oer = sum(oddA.^2)/sum(evenA.^2);
