function irr = spectralIrregularity(in, fs)
% A function which calculates the spectral irregularity of a signal.
%
% Usage: irr = spectralIrregularity(in, fs, method)
% 
% Return Values:
% (irr) is the spectral irregularity of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% find the partials
[f, a] = findSpectralPartials(in, fs, 50);

N = length(a);
irr = zeros(1, 3);

%case "k"
	s = 0;

	for i = 2:(N-1)
		s += abs(log10(a(i)) - log10(a(i-1) * a(i) * a(i+1)) / 3);
	end

	irr(1) = log10(20*s);

%case "j"
	s = sum(diff([a, 0]).^2);
	irr(2) = s / sum(a.^2);

%case "b"
	num = 0;
	den = 0;

	for i = 2:(N-1)
		temp = (a(i-1) + a(i) + a(i+1)) / 3;
		num += temp * abs(a(i) - temp);
		den += temp;
	end

	den *= sqrt(sum(a.^2));
	irr(3) = num/den;
