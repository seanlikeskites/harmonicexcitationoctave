function I = inharmonicity(in, fs, f0)
% A function which calculates the inharmonicity of a signal.
%
% Usage: I = inharmonicity(in, fs, f0)
%
% Return Values:
% (I) is the inharmonicity of the signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (f0) is an optional argument which can be used if you already know the fundamental of the signal.

% get frequencies of fundamental and other partials
if nargin < 3
	f0 = dftFundamental(in, fs);
	if f0 == 0
		f0 = zeroCrossingFundamental(in, fs);
	end
end
[partialFrequencies, partialAmplitudes] = findSpectralPartials(in, fs);

%% ignore partials less than or equal to the fundamental
%partialFrequencies = partialFrequencies(partialFrequencies >= f0*1.5);
%partialAmplitudes = partialAmplitudes(partialFrequencies >= f0*1.5);

% approximate which order partial a given frequency is
partialOrders = round(partialFrequencies/f0);

numerator = 2 * sum(abs(partialFrequencies - partialOrders*f0).*partialAmplitudes.^2);
denominator = f0 * sum(partialAmplitudes.^2);

if denominator == 0
	I = 2;
	return;
end

I = numerator/denominator;

% calculate inharmonicities of each partial
%partialInharmonicities = (partialFrequencies - partialOrders*f0)./((partialOrders.^2 - 1).*partialOrders*f0);

% calculate the weighted mean of the inharmonicities
%weightedInharmonicities = partialInharmonicities.*partialAmplitudes;
%I = sum(weightedInharmonicities)/sum(partialAmplitudes);
