function [m, v, s, k] = spectralMoments(in, fs)
% A function which calculates the spectral moments of a signal.
%
% Usage: [m, v, s, k] = spectralMoments(in, fs)
% 
% Return Values:
% (m) is the spectral centroid of the input signal.
% (v) is the spectral variance of the input signal.
% (s) is the spectral skewness of the input signal.
% (k) is the spectral kurtosis of the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.

% find the magnitude spectrum of the signal
complexDFT = fft(in);
magnitudeSpectrum = abs(complexDFT);

% get only the positive frequencies
signalLength = length(magnitudeSpectrum);
lastBin = floor(signalLength / 2) + 1;
posSpec = magnitudeSpectrum(1:lastBin);

if (rem(lastBin, 2) == 0)
	posSpec(2:end-1) *= 2;
else
	posSpec(2:end) *= 2;
end

% work out bin frequencies
binSize = fs/signalLength;
frequencies = 0:binSize:fs;
frequencies = frequencies(1:lastBin);

% total energy 
totalEnergy = sum(posSpec);

% calculate the centroid
m = sum(posSpec.*frequencies) / totalEnergy;

% calculate the variance
v = sum(posSpec.*((frequencies - m).^2)) / totalEnergy;

% calculate the skewness
s = sum(posSpec.*((frequencies - m).^3)) / (v^1.5 * totalEnergy);

% calculate the kurtosis
k = sum(posSpec.*((frequencies - m).^4)) / (v^2 * totalEnergy);
