function T = tristimulus(in, fs, f0)
% A function which calculates the tristimulus of a signal.
%
% Usage: T = tristimulus(in, fs, f0)
%
% Return Values:
% (T) is a vector tristimulus values of the signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (f0) is an optional argument which can be used if you already know the fundamental of the signal.

% get frequencies of fundamental and other partials
if nargin < 3
	f0 = dftFundamental(in, fs);
	if f0 == 0
		f0 = zeroCrossingFundamental(in, fs);
	end
end

a = harmonicLevels(in, fs, f0, 2);

first = a(1);
second = sum(a(2:4));
third = sum(a(5:end));
all = sum(a);

T = [first, second, third] / all;
