function hpr = harmonicParityRatio(in, fs, f0, skirt)
% A function which calculates the ratio between odd and even harmonics in a signal.
%
% Usage: hpr = harmonicParityRatio(in, fs, f0, skirt)
% 
% Return Values:
% (hpr) is the ratio between odd and even harmonics in the input signal.
%
% Arguments:
% (in) is the signal to be analysed.
% (fs) is the sample rate of that signal.
% (f0) is the fundamental frequency of that signal.
% (skirt) is for use when f0 will not lie in a DFT frequency bin. The algorithm will look this many frequency bins to each side to get a more accurate amplitude.

% get amplitudes of harmonics
amplitudes = harmonicLevels(in, fs, f0, skirt);

if length(amplitudes) < 2
	disp('I cannot compute the harmonic parity ratio for this signal. The sample rate is not high enough for there to be any harmonics other than the fundamental.');
	return;
end

% calculate the total power in both odd and even harmonics
oddHarmonics = sum(amplitudes(1:2:end).^2);
evenHarmonics = sum(amplitudes(2:2:end).^2);

% find the ratio
hpr = oddHarmonics/evenHarmonics;
