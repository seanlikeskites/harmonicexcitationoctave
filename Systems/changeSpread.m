function out = changeSpread(in, fs, f0, m)

out = zeros(1, length(m));
[c, ~, ~, ~] = spectralMoments(in ,fs);
cutoff = [0.8*c/f0, 1.2*c/f0];

% get the fundamental
If0 = isolateFundamental(in, fs, f0, 500);

% clip and filter the signal
nq = fs / 2;
hpfFreq = 0.8*c/nq;
[nldFilt.b, nldFilt.a] = butter(2, hpfFreq, 'high');
nldSig = filter(nldFilt.b, nldFilt.a, hardClip(If0, 10, 0.1));
lpfFreq = 1.2*c/nq;
[nldFilt.b, nldFilt.a] = butter(2, lpfFreq, 'low');
nldSig = filter(nldFilt.b, nldFilt.a, nldSig);

for i = 1:length(m)
	sig = in + m(i)*nldSig;
	[~, out(i), ~, ~] = spectralMoments(sig, fs);

	disp(i);
	fflush(stdout);
end
