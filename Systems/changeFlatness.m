function out = changeFlatness(in, fs, f0, n, m)

% find the partials
[f, a] = findHarmonicPartials(in, fs, f0, 0.1);
a ./= a(1)/2;
h = round(f./f0);
a = a(h <= 9);
h = h(h <= 9);

% make sure there are enough partials
if (n * 2) > length(h)
	error('Not enough partials in signal for that value of n');
end

% find the harmonics we will alter
[~, o] = sort(a);
L = a(o(1:n));
H_L = h(o(1:n));
H = a(o((end-n+1):end));
H_H = h(o((end-n+1):end));
harms = [H_L, H_H];
harmFlags = zeros(1, 9);
harmFlags(harms) = 1;
amps = zeros(1, 9);
amps(harms) = [L, H];

% resynthesise the signal
filters = cell(1, length(harms));

for i = 1:length(harms)
	filters{i} = [harms(i)-0.5, harms(i)+0.5];
end

sig = spectralShaper(in, fs, f0, harmFlags, amps, zeros(1, 8), @hardClip, {1, 1}, 0, 0, filters, 1);
[sigF, sigA] = findSpectralPartials(sig, fs);

% find the harmonics again
L_idx = zeros(1, n);
H_idx = zeros(1, n);

for i = 1:n
	[~, L_idx(i)] = min(abs(sigF - f0*H_L(i)));
	[~, H_idx(i)] = min(abs(sigF - f0*H_H(i)));
end

% work out the new amplitudes
A_L = sum(sigA(L_idx).^2);
A_H = sum(sigA(H_idx).^2);
noChange = A_L/A_H;

out = zeros(1, length(m));
A = sigA;

for i = 1:length(m)
	curM = sqrt(noChange^(1-m(i)));

	M_L = 1/curM;
	M_H = curM;

	A(L_idx) = M_L*sigA(L_idx);
	A(H_idx) = M_H*sigA(H_idx);

	out(i) = geomean(A.^2) / mean(A.^2);

	disp(i);
	fflush(stdout);
end
