function out = changeSlope(in, fs, f0, m)

out = zeros(1, length(m));

% resynthesise the signal
filters = cell(1, 9);

for i = 1:9
	filters{i} = [i-0.5, i+0.5];
end

a = harmonicLevels(in, fs, f0, 2);
a ./= a(1)/2;
harmGains = a(2:9);
sig = spectralShaper(in, fs, f0, ones(1, 9), a, zeros(1, 8), @hardClip, {1, 1}, 0, 0, filters, 1);

% find the partials
[freqs, amps] = findSpectralPartials(sig, fs);
amps = amps(freqs > f0-10);
freqs = freqs(freqs > f0-10);
f = freqs(freqs < f0*9+10);
harms = find(abs(f - f0*round(f/f0)) < 0.1*f0);

for i = 1:length(m)
	gains = 10.^(m(i).*log2(freqs(harms)./f0)./20);
	highGain = 10.^(m(i)*log2(10)/20);

	a = amps * highGain;
	a(harms) = amps(harms).*gains;

	A = sum(20*log10(a));
	B = sum(20*freqs.*log10(a));
	F = sum(freqs);
	G = sum(freqs.^2);
	N = length(a);

	out(i) = (N*B - F*A) / (N*G - F^2);

	disp(i);
	fflush(stdout);
end
