n = 4;
[clar, fs] = wavread('Systems/clarinet.wav');
clar = clar';
clarF0 = 195.75648;
[f, a] = findHarmonicPartials(clar, fs, clarF0, 0.1);
f = f(1:9);
a = a(1:9);

% find the harmonics we will alter
[~, o] = sort(a);
minId = o(1:n);
maxId = o(end-n+1:end);

A_L = sum(a(minId).^2);
A_H = sum(a(maxId).^2);
noChange = A_L/A_H;

m = 0:0.1:1;
res = zeros(1, length(m));

for i = 1:length(m)
	curM = sqrt(noChange^(1-m(i)));

	amps = a;
	amps(minId) /= curM;
	amps(maxId) *= curM;

	res(i) = geomean(amps.^2)/mean(amps.^2);
end
