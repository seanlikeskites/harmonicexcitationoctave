function out = parityShaper(in, fs, f0, nld, nldArgs, evenGain, oddGain, origFilter, origGain)

If0 = isolateFundamental(in, fs, f0, 50);

evens = nld(abs(in), nldArgs{:});
odds = evens.*sign(in);

evens *= evenGain;
evens -= mean(evens);
odds *= oddGain;

nq = fs/2;
hpfFreq = origFilter*f0/nq;
[b, a] = butter(2,  hpfFreq, 'high');
orig = origGain.*filter(b, a, in);

out = evens + odds + orig;
