%fs = 44100;
%nq = fs / 2;
%
%bass = wavread('Systems/bass.wav')';
%bass = 0.25*bass./max(abs(bass));
%bassF0 = 221.58714;
%[bassB, bassA] = butter(2, 9.5*bassF0/nq, 'low');
%bassFilt = filter(bassB, bassA, bass);
%
%clar = wavread('Systems/clarinet.wav')';
%clar = 0.25*clar./max(abs(clar));
%clarF0 = 195.75648;
%[clarB, clarA] = butter(2, 9.5*clarF0/nq, 'low');
%clarFilt = filter(clarB, clarA, clar);
%
%pian = wavread('Systems/piano.wav')';
%pian = 0.25*pian./max(abs(pian));
%pianF0 = 98.030766;
%[pianB, pianA] = butter(2, 9.5*pianF0/nq, 'low');
%pianFilt = filter(pianB, pianA, pian);
%
%syn = wavread('Systems/synth.wav')';
%syn = 0.25*syn./max(abs(syn));
%synF0 = 55.235179;
%[synB, synA] = butter(2, 9.5*synF0/nq, 'low');
%synFilt = filter(synB, synA, syn);

signals = {'Cello', 'Clarinet', 'Synthesised', 'Piano'};

%% move the centroids
%disp('Centroids');
%gains = 0:0.01:1;
%centroids = zeros(4, length(gains));
%centroids(1, :) = changeCentroid(bassFilt, fs, bassF0, gains);
%centroids(2, :) = changeCentroid(clarFilt, fs, clarF0, gains);
%centroids(3, :) = changeCentroid(pianFilt, fs, pianF0, gains);
%centroids(4, :) = changeCentroid(synFilt, fs, synF0, gains);
%save("Systems/MoveCentroids.mat", "centroids");
%%load("Systems/MoveCentroids.mat");
%hideFigures(1);
%plot(gains, centroids([1, 2, 4, 3], :)/1000);
%legend(signals, 'location', 'northwest');
%ylabel('Spectral Centroid (kHz)');
%xlabel('Parameter Setting');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveCentroids.tex',
%'-depslatex', '-F:20');
%
%% change the flatnesses
%disp('Flatness');
%args = 0:0.01:1.5;
%flatnesses = zeros(4, length(args));
%flatnesses(1, :) = changeFlatness(bass, fs, bassF0, 3, args);
%flatnesses(2, :) = changeFlatness(clar, fs, clarF0, 3, args);
%flatnesses(3, :) = changeFlatness(pian, fs, pianF0, 3, args);
%flatnesses(4, :) = changeFlatness(syn, fs, synF0, 3, args);
%save("Systems/MoveFlatnesses.mat", "flatnesses");
%%load("Systems/MoveFlatnesses.mat");
%hideFigures(2);
%plot(args, flatnesses([1, 2, 4, 3], :));
%xlim([0, 1.5]);
%legend(signals, 'location', 'northeast');
%ylabel('Spectral Flatness');
%xlabel('Parameter Setting');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveFlatnesses.tex',
%'-depslatex', '-F:20');
%
%% change the parity ratios
%disp('Parity');
gains = 0:0.01:1;
%parities = zeros(4, length(gains));
%parities(1, :) = changeParity(bass, fs, bassF0, gains);
%parities(2, :) = changeParity(clar, fs, clarF0, gains);
%parities(3, :) = changeParity(pian, fs, pianF0, gains);
%parities(4, :) = changeParity(syn, fs, synF0, gains);
%save("Systems/MoveParities.mat", "parities");
load("Systems/MoveParities.mat");
hideFigures(3);
plot(gains, parities([1, 2, 4, 3], :));
ylim([0, 60]);
legend(signals, 'location', 'northwest');
ylabel('Odd to Even Harmonic Ratio');
xlabel('Parameter Setting');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveParities.tex', 
'-depslatex', '-F:20');
%
%% change the tristimulus 1s
%disp('Tri1');
%gains = 0:0.1:5;
%tri1s = zeros(4, length(gains));
%tri1s(1, :) = changeTri1(bass, fs, bassF0, gains);
%tri1s(2, :) = changeTri1(clar, fs, clarF0, gains);
%tri1s(3, :) = changeTri1(pian, fs, pianF0, gains);
%tri1s(4, :) = changeTri1(syn, fs, synF0, gains);
%save("Systems/MoveTristimulus1.mat", "tri1s");
%%load("Systems/MoveTristimulus1.mat")
%hideFigures(4);
%plot(gains, tri1s([1, 2, 4, 3], :));
%ylim([0, 0.9]);
%legend(signals, 'location', 'northwest');
%ylabel('First Tristimulus');
%xlabel('$f_{0}$ Gain');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveTristimulus1.tex',
%'-depslatex', '-F:20');
%
%% change the tristimulus 2s
%disp('Tri2');
gains = 0:0.1:5;
%tri2s = zeros(4, length(gains));
%tri2s(1, :) = changeTri2(bass, fs, bassF0, gains);
%tri2s(2, :) = changeTri2(clar, fs, clarF0, gains);
%tri2s(3, :) = changeTri2(pian, fs, pianF0, gains);
%tri2s(4, :) = changeTri2(syn, fs, synF0, gains);
%save("Systems/MoveTristimulus2.mat", "tri2s");
load("Systems/MoveTristimulus2.mat")
hideFigures(5);
plot(gains, tri2s([1, 2, 4, 3], :));
ylim([0, 0.8]);
legend(signals, 'location', 'northwest');
ylabel('Second Tristimulus');
xlabel('Band Gain');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveTristimulus2.tex',
'-depslatex', '-F:20');
%
%% change the tristimulus 3s
%disp('Tri3');
gains = 0:0.1:5;
%tri3s = zeros(4, length(gains));
%tri3s(1, :) = changeTri3(bass, fs, bassF0, gains);
%tri3s(2, :) = changeTri3(clar, fs, clarF0, gains);
%tri3s(3, :) = changeTri3(pian, fs, pianF0, gains);
%tri3s(4, :) = changeTri3(syn, fs, synF0, gains);
%save("Systems/MoveTristimulus3.mat", "tri3s");
load("Systems/MoveTristimulus3.mat");
hideFigures(6);
plot(gains, tri3s([1, 2, 4, 3], :));
ylim([0, 0.9]);
legend(signals, 'location', 'northwest');
ylabel('Third Tristimulus');
xlabel('Band Gain');
print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveTristimulus3.tex',
'-depslatex', '-F:20');
%
%% change the inharmonicities
%disp('Inharmonicity')
%gains = 0:0.00005:0.001;
%inharms = zeros(4, length(gains));
%inharms(1, :) = changeInharmonicity(bass, fs, bassF0, gains);
%inharms(2, :) = changeInharmonicity(clar, fs, clarF0, gains);
%inharms(3, :) = changeInharmonicity(pian, fs, pianF0, gains);
%inharms(4, :) = changeInharmonicity(syn, fs, synF0, gains);
%save("Systems/MoveInharmonicities.mat", "inharms");
%%load("Systems/MoveInharmonicities.mat");
%hideFigures(7);
%plot(gains, inharms([1, 2, 4, 3], :));
%legend(signals, 'location', 'northwest');
%ylabel('Inharmonicity');
%xlabel('Inharmonicity Factor');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveInharmonicities.tex',
%'-depslatex', '-F:20');
%
%% change the irregularities
%disp('Irregularity')
%m = -1:0.01:1;
%irregs = zeros(3, length(m), 4);
%irregs(:, :, 1) = changeIrregularity(bass, fs, bassF0, m);
%irregs(:, :, 2) = changeIrregularity(clar, fs, clarF0, m);
%irregs(:, :, 3) = changeIrregularity(pian, fs, pianF0, m);
%irregs(:, :, 4) = changeIrregularity(syn, fs, synF0, m);
%save("Systems/MoveIrregularities.mat", "irregs");
%%load("Systems/MoveIrregularities.mat");
%hideFigures(8);
%plot(m, squeeze(irregs(1, :, [1, 2, 4, 3])));
%%xlim([-0.5, 0.5]);
%legend(signals, 'location', 'northeast');
%ylabel('Krimphoff Irregularity');
%xlabel('Parameter Setting');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveIrregularitiesK.tex',
%'-depslatex', '-F:20');
%
%hideFigures(9);
%plot(m, squeeze(irregs(2, :, [1, 2, 4, 3])));
%%xlim([-0.5, 0.5]);
%legend(signals, 'location', 'southwest');
%ylabel('Jensen Irregularity');
%xlabel('Parameter Setting');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveIrregularitiesJ.tex',
%'-depslatex', '-F:20');
%
%hideFigures(10);
%plot(m, squeeze(irregs(3, :, [1, 2, 4, 3])));
%%xlim([-0.5, 0.5]);
%legend(signals, 'location', 'northeast');
%ylabel('Beauchamp Irregularity');
%xlabel('Parameter Setting');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveIrregularitiesB.tex',
%'-depslatex', '-F:20');
%
%% change the slopes
%disp('Slope')
%gains = -10:0.5:10;
%slopes = zeros(4, length(gains));
%slopes(1, :) = changeSlope(bass, fs, bassF0, gains);
%slopes(2, :) = changeSlope(clar, fs, clarF0, gains);
%slopes(3, :) = changeSlope(pian, fs, pianF0, gains);
%slopes(4, :) = changeSlope(syn, fs, synF0, gains);
%save("Systems/MoveSlopes.mat", "slopes");
%%load("Systems/MoveSlopes.mat");
%hideFigures(11);
%plot(gains, slopes([1, 2, 4, 3], :));
%ylim([-0.01, 0]);
%legend(signals, 'location', 'southeast');
%ylabel('Tilt Gradient');
%xlabel('Parameter Setting');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveSlopes.tex',
%'-depslatex', '-F:20');
%
%% change the skewnesses
%disp('Skewness')
%gains = -10:0.5:10;
%skews = zeros(4, length(gains));
%skews(1, :) = changeSkew(bass, fs, bassF0, gains);
%skews(2, :) = changeSkew(clar, fs, clarF0, gains);
%skews(3, :) = changeSkew(pian, fs, pianF0, gains);
%skews(4, :) = changeSkew(syn, fs, synF0, gains);
%save("Systems/MoveSkewnesses.mat", "skews");
%%load("Systems/MoveSkewnesses.mat");
%hideFigures(12);
%plot(gains, skews([1, 2, 4, 3], :));
%legend(signals, 'location', 'northeast');
%ylabel('Spectral Skewness');
%xlabel('Tilt Gradient');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveSkewnesses.tex',
%'-depslatex', '-F:20');
%
%% change the spreads
%disp('Spread')
%gains = 0:0.1:10;
%spreads = zeros(4, length(gains));
%spreads(1, :) = changeSpread(bass, fs, bassF0, gains);
%spreads(2, :) = changeSpread(clar, fs, clarF0, gains);
%spreads(3, :) = changeSpread(pian, fs, pianF0, gains);
%spreads(4, :) = changeSpread(syn, fs, synF0, gains);
%save("Systems/MoveSpreads.mat", "spreads");
%%load("Systems/MoveSpreads.mat");
%hideFigures(13);
%plot(gains, spreads([1, 2, 4, 3], :)/1000000);
%legend(signals, 'location', 'northeast');
%ylabel('Spectral Spread (kHz\super{2})');
%xlabel('Band Gain');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveSpreads.tex',
%'-depslatex', '-F:20');
%
%% change the kurtosises
%disp('Kurtosis')
%gains = 0:0.1:10;
%kurts = zeros(4, length(gains));
%kurts(1, :) = changeKurtosis(bass, fs, bassF0, gains);
%kurts(2, :) = changeKurtosis(clar, fs, clarF0, gains);
%kurts(3, :) = changeKurtosis(pian, fs, pianF0, gains);
%kurts(4, :) = changeKurtosis(syn, fs, synF0, gains);
%save("Systems/MoveKurtoses.mat", "kurts");
%%load("Systems/MoveKurtoses.mat");
%hideFigures(14);
%plot(gains, kurts([1, 2, 4, 3], :));
%legend(signals, 'location', 'northwest');
%ylabel('Spectral Kurtosis');
%xlabel('Band Gain');
%print('/home/sean/Documents/ResearchWork/HarmonicExcitationOctave/Systems/MoveKurtoses.tex',
%'-depslatex', '-F:20');
