function out = changeParity(in, fs, f0, m)

% calculate the odd and even harmonics
If0 = isolateFundamental(in, fs, f0, 50);
odds = hardClip(If0, 10, 0.1);
evens = abs(odds);
evens -= mean(evens);

% filter the original signal
nq = fs/2;
hpfFreq = 9.5*f0/nq;
[b, a] = butter(2,  hpfFreq, 'high');
orig = filter(b, a, in);

% filter the harmonics
nq = fs/2;
lpfFreq = 9.5*f0/nq;
[b, a] = butter(2,  lpfFreq, 'low');
odds = filter(b, a, odds);
evens = filter(b, a, evens);

out = zeros(1, length(m));

for i = 1:length(m)
	om = m(i);
	em = 1 - m(i);

	%sig = parityShaper(in, fs, f0, @hardClip, {10, 0.1}, em, om, 9.5, 1);
	sig = orig + em*evens + om*odds;

	out(i) = oddEvenRatio(sig, fs, f0);

	disp(i);
	fflush(stdout);
end
