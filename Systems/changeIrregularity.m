function out = changeIrregularity(in, fs, f0, m)

out = zeros(3, length(m));
a = harmonicLevels(in, fs, f0, 2);
a ./= a(1)/2;
harmGains = a(1:9);

filters = {[0.5, 1.5], [1.5, 2.5], [2.5, 3.5], [3.5, 4.5], [4.5, 5.5], [5.5, 6.5], [6.5, 7.5], [7.5, 8.5], [8.5, 9.5]};
sig = spectralShaper(in, fs, f0, ones(1, 9), harmGains, zeros(1, 8), @hardClip, {1, 1}, 0, 0, filters, 1);

amps = harmonicLevels(sig, fs, f0, 2);
a = amps;
mE = mean(amps(1:9));
mT = mE./a(1:9);

for n = 1:length(m)
	%%%% this results in negative amplitudes!
	%%%% a(1:9) = amps(1:9) - m(n)*(amps(1:9) - mE);

	%filter
	gain = mT.^(m(n));
	a(1:9) = gain.*amps(1:9);

	%hideFigures(n);
	%bar(a);

	N = length(a);
	%case "k"
	s = 0;

	for i = 2:(N-1)
		%s += abs(log10(a(i)) - log10(a(i-1) * a(i) * a(i+1)) / 3);
		s += abs(a(i) - (a(i-1) + a(i) + a(i+1)) / 3);
	end

	%out(1, n) = log10(20*s);
	out(1, n) = s;

	%case "j"
	s = sum(diff([a, 0]).^2);
	out(2, n) = s / sum(a.^2);

	%case "b"
	num = 0;
	den = 0;

	for i = 2:(N-1)
		temp = (a(i-1) + a(i) + a(i+1)) / 3;
		num += temp * abs(a(i) - temp);
		den += temp;
	end

	den *= sqrt(sum(a.^2));
	out(3, n) = num/den;

	disp(n);
	fflush(stdout);
end
