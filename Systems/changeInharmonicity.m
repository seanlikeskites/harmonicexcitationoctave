function out = changeInharmonicity(in, fs, f0, m)

out = zeros(1, length(m));

a = harmonicLevels(in, fs, f0, 2);
a ./= a(1)/2;

filters = cell(1, 8);

for i = 1:8
	filters{i} = [i+0.5, i+1.5];
end

harmonic = spectralShaper(in, fs, f0, [0, ones(1, 8)], a, zeros(1, 8), @hardClip, {1, 1}, 0, 0, filters, 1);
[hF, hA] = findSpectralPartials(harmonic, fs);
hO = round(hF/f0);
movingHarms = (hO < 10) & (hO > 1);
movingOrders = hO(movingHarms);

for i = 1:length(m)
	B = 1 + m(i)*((movingOrders).^2 - 1);
	hF(movingHarms) = movingOrders.*f0.*B;

	num = 2 * sum(abs(hF - hO*f0).*hA.^2);
	den = f0 * sum(hA.^2);

	out(i) = num/den;

	disp(i);
	fflush(stdout);
end
