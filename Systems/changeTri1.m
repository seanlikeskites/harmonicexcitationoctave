function out = changeTri1(in, fs, f0, m)

% get the findamental
If0 = isolateFundamental(in, fs, f0, 50);

% filter the original signal
nq = fs/2;
hpfFreq = 1.5*f0/nq;
[b, a] = butter(2,  hpfFreq, 'high');
orig = filter(b, a, in);

out = zeros(1, length(m));

for i = 1:length(m)
	sig = orig + m(i)*If0;
	%sig = spectralShaper(in, fs, f0, [1, zeros(1, 8)], [m(i), zeros(1, 8)], zeros(1, 8), @hardClip, {1, 1}, 0, 0,
	%                     {[0.5, 1.5]}, 1);

	t = tristimulus(sig, fs, f0);
	out(i) = t(1);

	disp(i);
	fflush(stdout);
end
