function out = spectralShaper(in, fs, f0, harms, gains, shifts, nld, nldArgs, nldGain, nldFilter, harmFilters, origGain)

If0 = isolateFundamental(in, fs, f0, 500);
hf0 = hilbert(If0);
af0 = abs(hf0);
pf0 = arg(hf0);

% do the individual harmonics
harmonics = zeros(9, length(in));

if harms(1)
	harmonics(1, :) = gains(1).*If0;
end

ts = 1/fs;
t = ts*(0:(length(in) - 1));

for i = 2:9
	if harms(i)
		shiftFreq = f0*shifts(i - 1);
		shifter = exp(2*pi*j*shiftFreq*t);
		harmonics(i, :) = real(shifter.*hilbert(gains(i).*af0.*cos(i.*pf0)));
	end
end

nq = fs / 2;
hpfFreq = nldFilter(1)*f0/nq;
[nldFilt.b, nldFilt.a] = butter(2, hpfFreq, 'high');
nldSig = filter(nldFilt.b, nldFilt.a, nld(If0, nldArgs{:}));

if (length(nldFilter) > 1)
	lpfFreq = nldFilter(2)*f0/nq;
	[nldFilt.b, nldFilt.a] = butter(2, lpfFreq, 'low');
	nldSig = filter(nldFilt.b, nldFilt.a, nldSig);
end

nldSig *= nldGain;

harmonicNotches = cell(1, length(harmFilters));
filtered = in;

for i = 1:length(harmFilters)
	lowFreq = harmFilters{i}(1)*f0/nq;
	highFreq = harmFilters{i}(2)*f0/nq;
	[temp.b, temp.a] = butter(2, [lowFreq, highFreq], 'stop');
	harmonicNotches(i) = temp;

	filtered = filter(temp.b, temp.a, filtered);
end

out = origGain*filtered + nldSig + sum(harmonics, 1);
