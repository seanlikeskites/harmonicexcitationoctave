function out = changeSkew(in, fs, f0, m)

out = zeros(1, length(m));

% resynthesise the signal
filters = cell(1, 9);

for i = 1:9
	filters{i} = [i-0.5, i+0.5];
end

a = harmonicLevels(in, fs, f0, 2);
a ./= a(1)/2;
freqs = f0*(1:9);

centroid = spectralMoments(in, fs);

for i = 1:length(m)
	gains = 10.^(m(i).*log2(freqs/centroid)./20);

	sig = spectralShaper(in, fs, f0, ones(1, 9), a(1:9).*gains, zeros(1, 8), @hardClip, {1, 1}, 0, 0, filters, 1);
	[~, ~, out(i), ~] = spectralMoments(sig, fs);

	disp(i);
	fflush(stdout);
end
