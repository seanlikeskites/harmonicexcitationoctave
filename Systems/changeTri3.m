function out = changeTri3(in, fs, f0, m)

centroids = zeros(1, length(m));

% get the fundamental
If0 = isolateFundamental(in, fs, f0, 500);

% clip and filter the signal
nq = fs / 2;
hpfFreq = 6.5*f0/nq;
[nldFilt.b, nldFilt.a] = butter(2, hpfFreq, 'high');
nldSig = 3*filter(nldFilt.b, nldFilt.a, hardClip(If0, 10, 0.1));

out = zeros(1, length(m));

for i = 1:length(m)
	sig = in + m(i)*nldSig;
	%sig = spectralShaper(in, fs, f0, zeros(1, 9), zeros(1, 9), zeros(1, 8), 
	%		     @hardClip, {10, 0.1}, m(i), 4.5, {}, 1);

	t = tristimulus(sig, fs, f0);
	out(i) = t(3);

	disp(i);
	fflush(stdout);
end

