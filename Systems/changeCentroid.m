function centroids = changeCentroid(in, fs, f0, m)

centroids = zeros(1, length(m));
%outputs = zeros(length(m), length(in));

% get the fundamental
If0 = isolateFundamental(in, fs, f0, 500);

% clip and filter the signal
nq = fs / 2;
hpfFreq = 9.5*f0/nq;
[nldFilt.b, nldFilt.a] = butter(2, hpfFreq, 'high');
nldSig = filter(nldFilt.b, nldFilt.a, hardClip(If0, 10, 0.1));

for i = 1:length(m)
	om = 1 - m(i);

	sig = m(i)*nldSig + om*in;
	%sig = spectralShaper(in, fs, f0, zeros(1, 9), zeros(1, 9), zeros(1, 8), @hardClip, {10, 0.1}, m(i), 9.5, {}, om);

	centroids(i) = spectralMoments(sig, fs);

	disp(i);
	fflush(stdout);
end
