function out = changeTri2(in, fs, f0, m)

% get the findamental
If0 = isolateFundamental(in, fs, f0, 50);
hf0 = hilbert(If0);
af0 = abs(hf0);
pf0 = arg(hf0);

% generate the harmonics
a = harmonicLevels(in, fs, f0, 2);
a ./= a(1)/2;
harms = zeros(1, length(in));

for i = 2:4
	harms += a(i)*af0.*cos(i*pf0);
end

% filter the input
nq = fs/2;

for i = 2:4
	lowFreq = (i - 0.5)*f0/nq;
	highFreq = (i + 0.5)*f0/nq;
	[temp.b, temp.a] = butter(2, [lowFreq, highFreq], 'stop');
	in = filter(temp.b, temp.a, in);
end

out = zeros(1, length(m));

for i = 1:length(m)
	sig = in + m(i)*harms;
	%sig = spectralShaper(in, fs, f0, [0, ones(1, 3), zeros(1, 5)], [0, m(i)*ones(1, 3), zeros(1, 5)], zeros(1, 8), 
	%		     @hardClip, {1, 1}, 0, 0, {[1.5, 2.5], [2.5, 3.5], [3.5, 4.5]}, 1);

	t = tristimulus(sig, fs, f0);
	out(i) = t(2);

	disp(i);
	fflush(stdout);
end
